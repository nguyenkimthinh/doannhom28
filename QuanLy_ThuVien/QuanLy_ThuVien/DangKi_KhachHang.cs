﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLy_ThuVien
{
    public partial class DangKi_KhachHang : Form
    {
        string Connect1 = @"Data Source=DESKTOP-V6NV63V\SQLEXPRESS;Initial Catalog=QLThuVien;Integrated Security=True";
        SqlCommand Comm1;
        public DangKi_KhachHang()
        {
            InitializeComponent();
        }
        private void btnDKTaiKhoan_Click(object sender, EventArgs e)
        {
            if (txtHoTen.Text == "")
            {
                MessageBox.Show("Bạn chưa nhập vào họ tên");
                txtHoTen.Focus();
            }
            else if (txtGioiTinh.Text == "")
            {
                MessageBox.Show("Bạn chưa nhập vào giới tính");
                txtGioiTinh.Focus();
            }
            else if (txtEmail.Text == "")
            {
                MessageBox.Show("Bạn chưa nhập vào Email");
                txtEmail.Focus();
            }
            else if (txtPhone.Text == "")
            {
                MessageBox.Show("Bạn chưa nhập vào số điện thoại");
                txtPhone.Focus();
            }
            else if (txtTaiKhoan.Text == "")
            {
                MessageBox.Show("Bạn chưa nhập vào tài khoản");
                txtTaiKhoan.Focus();
            }
            else if (txtMatKhau.Text == "")
            {
                MessageBox.Show("Bạn chưa nhập vào mật khẩu");
                txtMatKhau.Focus();
            }
            else if (txtXNMatKhau.Text == "")
            {
                MessageBox.Show("Bạn chưa nhập vào xác nhận mật khẩu");
                txtXNMatKhau.Focus();
            }
            else if (txtMatKhau.Text != txtXNMatKhau.Text)
            {
                MessageBox.Show("Mật khẩu và xác nhận mật khẩu phải giống nhau. Xin vui lòng nhập lại!");
                txtXNMatKhau.Focus();
                txtXNMatKhau.SelectAll();
            }
            /// kiểm tra mật khẩu phải có 7 kí tự gồm chữ và số
            else
            {
                NguoiDung nd = new NguoiDung(txtHoTen.Text, txtGioiTinh.Text, txtEmail.Text, txtPhone.Text, txtTaiKhoan.Text, txtMatKhau.Text);
                if (nd.KiemTraDinhDangMatKhau() == true)
                {
                    try
                    {
                        SqlConnection Conn1;
                        Conn1 = new SqlConnection(Connect1);
                        
                        Conn1.Open();
                        
                        string query2 = "INSERT [QLThuVien].[dbo].[Khachhang] VALUES ([dbo].UF_AUTO_maKH(), @hoten, @gioitinh, @email, @sodt)";
                        Comm1 = new SqlCommand(query2, Conn1);
                        string query1 = "INSERT [QLThuVien].[dbo].[TaiKhoan] VALUES (@acc, @mk, NULL, 1)";
                        Comm1 = new SqlCommand(query1, Conn1);
                        Comm1.Parameters.Add(new SqlParameter("@acc", txtTaiKhoan.Text));
                        Comm1.Parameters.Add(new SqlParameter("@mk", txtMatKhau.Text));
                        Comm1.Parameters.Add(new SqlParameter("@hoten", txtHoTen.Text));
                        Comm1.Parameters.Add(new SqlParameter("@gioitinh", txtGioiTinh.Text));
                        Comm1.Parameters.Add(new SqlParameter("@email", txtEmail.Text));
                        Comm1.Parameters.Add(new SqlParameter("@sodt", txtPhone.Text));
                        Comm1.ExecuteNonQuery();
                        MessageBox.Show("Đăng kí tài khoản thành công!");
                        Conn1.Close();
                    }
                    catch (Exception ex1)
                    {
                        MessageBox.Show(ex1.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Mật khẩu phải có tối thiểu 8 kí tự và có cả chữ và số. xin vui lòng nhập lại!");
                }
            }
        }
    }
}
