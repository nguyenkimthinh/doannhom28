﻿namespace QuanLy_ThuVien
{
    partial class DangNhap_KhachHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDangKi = new System.Windows.Forms.Label();
            this.labInCorrect = new System.Windows.Forms.Label();
            this.Cancel = new System.Windows.Forms.Button();
            this.Login = new System.Windows.Forms.Button();
            this.checkHienThiPass = new System.Windows.Forms.CheckBox();
            this.labPassword = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.labUserName = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.PicPassword = new System.Windows.Forms.PictureBox();
            this.PicUserName = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicUserName)).BeginInit();
            this.SuspendLayout();
            // 
            // txtDangKi
            // 
            this.txtDangKi.AutoSize = true;
            this.txtDangKi.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDangKi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtDangKi.Location = new System.Drawing.Point(43, 270);
            this.txtDangKi.Name = "txtDangKi";
            this.txtDangKi.Size = new System.Drawing.Size(99, 19);
            this.txtDangKi.TabIndex = 23;
            this.txtDangKi.Text = "Đăng kí mới?";
            this.txtDangKi.Click += new System.EventHandler(this.txtDangKi_Click);
            // 
            // labInCorrect
            // 
            this.labInCorrect.AutoSize = true;
            this.labInCorrect.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labInCorrect.ForeColor = System.Drawing.Color.Red;
            this.labInCorrect.Location = new System.Drawing.Point(89, 254);
            this.labInCorrect.Name = "labInCorrect";
            this.labInCorrect.Size = new System.Drawing.Size(0, 19);
            this.labInCorrect.TabIndex = 19;
            // 
            // Cancel
            // 
            this.Cancel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancel.Location = new System.Drawing.Point(441, 280);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 33);
            this.Cancel.TabIndex = 18;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // Login
            // 
            this.Login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Login.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Login.ForeColor = System.Drawing.Color.Black;
            this.Login.Location = new System.Drawing.Point(328, 280);
            this.Login.Name = "Login";
            this.Login.Size = new System.Drawing.Size(75, 33);
            this.Login.TabIndex = 17;
            this.Login.Text = "Login";
            this.Login.UseVisualStyleBackColor = false;
            this.Login.Click += new System.EventHandler(this.Login_Click);
            // 
            // checkHienThiPass
            // 
            this.checkHienThiPass.AutoSize = true;
            this.checkHienThiPass.Location = new System.Drawing.Point(205, 173);
            this.checkHienThiPass.Name = "checkHienThiPass";
            this.checkHienThiPass.Size = new System.Drawing.Size(102, 17);
            this.checkHienThiPass.TabIndex = 16;
            this.checkHienThiPass.Text = "Show Password";
            this.checkHienThiPass.UseVisualStyleBackColor = true;
            this.checkHienThiPass.CheckedChanged += new System.EventHandler(this.checkHienThiPass_CheckedChanged);
            // 
            // labPassword
            // 
            this.labPassword.AutoSize = true;
            this.labPassword.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.labPassword.Location = new System.Drawing.Point(175, 106);
            this.labPassword.Name = "labPassword";
            this.labPassword.Size = new System.Drawing.Size(90, 22);
            this.labPassword.TabIndex = 15;
            this.labPassword.Text = "Password";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(252, 131);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(181, 20);
            this.txtPassword.TabIndex = 14;
            this.txtPassword.UseSystemPasswordChar = true;
            this.txtPassword.TextChanged += new System.EventHandler(this.txtPassword_TextChanged);
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(252, 73);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(181, 20);
            this.txtUserName.TabIndex = 13;
            this.txtUserName.TextChanged += new System.EventHandler(this.txtUserName_TextChanged);
            // 
            // labUserName
            // 
            this.labUserName.AutoSize = true;
            this.labUserName.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.labUserName.Location = new System.Drawing.Point(175, 36);
            this.labUserName.Name = "labUserName";
            this.labUserName.Size = new System.Drawing.Size(96, 22);
            this.labUserName.TabIndex = 12;
            this.labUserName.Text = "UserName";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = global::QuanLy_ThuVien.Properties.Resources.download;
            this.pictureBox1.Location = new System.Drawing.Point(19, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 168);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // PicPassword
            // 
            this.PicPassword.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PicPassword.Image = global::QuanLy_ThuVien.Properties.Resources.pass;
            this.PicPassword.Location = new System.Drawing.Point(222, 131);
            this.PicPassword.Name = "PicPassword";
            this.PicPassword.Size = new System.Drawing.Size(32, 24);
            this.PicPassword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicPassword.TabIndex = 21;
            this.PicPassword.TabStop = false;
            // 
            // PicUserName
            // 
            this.PicUserName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PicUserName.Image = global::QuanLy_ThuVien.Properties.Resources.depositphotos_182720668_stock_illustration_mail_envelope_icon_symbol_of;
            this.PicUserName.Location = new System.Drawing.Point(221, 61);
            this.PicUserName.Name = "PicUserName";
            this.PicUserName.Size = new System.Drawing.Size(33, 32);
            this.PicUserName.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicUserName.TabIndex = 20;
            this.PicUserName.TabStop = false;
            // 
            // DangNhap_KhachHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 350);
            this.Controls.Add(this.txtDangKi);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.PicPassword);
            this.Controls.Add(this.PicUserName);
            this.Controls.Add(this.labInCorrect);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.Login);
            this.Controls.Add(this.checkHienThiPass);
            this.Controls.Add(this.labPassword);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.labUserName);
            this.Name = "DangNhap_KhachHang";
            this.Text = "DangNhap";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicUserName)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label txtDangKi;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox PicPassword;
        private System.Windows.Forms.PictureBox PicUserName;
        private System.Windows.Forms.Label labInCorrect;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Button Login;
        private System.Windows.Forms.CheckBox checkHienThiPass;
        private System.Windows.Forms.Label labPassword;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label labUserName;
    }
}