﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLy_ThuVien
{
    public partial class DangNhap_Admin : Form
    {
        Dashboard_Admin dash_Ad;
        DangKi_Admin DK_Ad;
        string Connect = @"Data Source=DESKTOP-V6NV63V\SQLEXPRESS;Initial Catalog=QLThuVien;Integrated Security=True";
        SqlConnection Conn1;
        SqlCommand Comm1;
        public DangNhap_Admin()
        {
            InitializeComponent();
        }

        private void txtUserName_Admin_TextChanged(object sender, EventArgs e)
        {
            if (txtUserName_Admin.Text == "")
            {
                MessageBox.Show("Bạn chưa nhập tên đăng nhập!");
                txtUserName_Admin.Focus();
            }
        }

        private void txtPassword_Admin_TextChanged(object sender, EventArgs e)
        {
            if (txtPassword_Admin.Text == "")
            {
                MessageBox.Show("Bạn chưa nhập mật khẩu!");
                txtPassword_Admin.Focus();
            }
        }

        private void checkHienThiPass_CheckedChanged(object sender, EventArgs e)
        {
            if (checkHienThiPass.Checked)
            {
                txtPassword_Admin.UseSystemPasswordChar = false;
            }
            else
            {
                txtPassword_Admin.UseSystemPasswordChar = true;
            }
        }

        private void Login_Admind_Click(object sender, EventArgs e)
        {
            try
            {
                Conn1 = new SqlConnection(Connect);
                Conn1.Open();
                string Sql = "SELECT COUNT(*) FROM [QLThuVien].[dbo].[TaiKhoan] WHERE UserName=@TaiKhoan AND MyPassword=@MatKhau AND PhanQuyen=0";
                Comm1 = new SqlCommand(Sql, Conn1);
                Comm1.Parameters.Add(new SqlParameter("@TaiKhoan", txtUserName_Admin.Text));
                Comm1.Parameters.Add(new SqlParameter("@MatKhau", txtPassword_Admin.Text));
                int x = (int)Comm1.ExecuteScalar();
                if (x == 1)
                {
                    //Đăng nhập thành công.
                    //MessageBox.Show("Success!");
                    this.Hide();
                    dash_Ad = new Dashboard_Admin();
                    dash_Ad.Show();
                }
                else
                {
                    //Đăng nhập thất bai.
                    //MessageBox.Show("Fail!!!");
                    labInCorrect.Text = "UserName hoặc Password bạn nhập chưa đúng. Vui lòng nhập lại!";
                    labInCorrect.Text = "Hoặc bạn không phải là admin!";
                    txtPassword_Admin.Text = "";
                    txtPassword_Admin.Text = "";
                    txtUserName_Admin.Focus();
                }

                Conn1.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Cancel_Admin_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtDangKi_Admin_Click(object sender, EventArgs e)
        {
            this.Hide();
            DK_Ad = new DangKi_Admin();
            DK_Ad.Show();
        }
    }
}
