﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLy_ThuVien
{
    public partial class DangNhap_KhachHang : Form
    {
        Dashboard_KhachHang dash_KH;
        DangKi_KhachHang DK;
        string Connect = @"Data Source=DESKTOP-V6NV63V\SQLEXPRESS;Initial Catalog=QLThuVien;Integrated Security=True";
        SqlConnection Conn;
        SqlCommand Comm;
        public DangNhap_KhachHang()
        {
            InitializeComponent();
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            if (txtUserName.Text == "")
            {
                MessageBox.Show("Bạn chưa nhập tên đăng nhập!");
                txtUserName.Focus();
            }
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            if (txtPassword.Text == "")
            {
                MessageBox.Show("Bạn chưa nhập mật khẩu!");
                txtPassword.Focus();
            }
        }

        private void Login_Click(object sender, EventArgs e)
        {
            try
            {
                Conn = new SqlConnection(Connect);
                Conn.Open();
                string Sql = "SELECT COUNT(*) FROM [QLThuVien].[dbo].[TaiKhoan] WHERE UserName=@TaiKhoan AND MyPassword=@MatKhau AND PhanQuyen=1";
                Comm = new SqlCommand(Sql, Conn);
                Comm.Parameters.Add(new SqlParameter("@TaiKhoan", txtUserName.Text));
                Comm.Parameters.Add(new SqlParameter("@MatKhau", txtPassword.Text));
                int x = (int)Comm.ExecuteScalar();
                if (x == 1)
                {
                    //Đăng nhập thành công.
                    //MessageBox.Show("Success!");
                    this.Hide();
                    dash_KH = new Dashboard_KhachHang();
                    dash_KH.Show();
                }               
               else
                {
                    //Đăng nhập thất bai.
                    //MessageBox.Show("Fail!!!");
                    labInCorrect.Text = "UserName hoặc Password bạn nhập chưa đúng. Vui lòng nhập lại!";
                    txtPassword.Text = "";
                    txtPassword.Text = "";
                    txtUserName.Focus();
                }
                Conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void checkHienThiPass_CheckedChanged(object sender, EventArgs e)
        {
            if (checkHienThiPass.Checked)
            {
                txtPassword.UseSystemPasswordChar = false;
            }
            else
            {
                txtPassword.UseSystemPasswordChar = true;
            }
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtDangKi_Click(object sender, EventArgs e)
        {
            this.Hide();
            DK = new DangKi_KhachHang();
            DK.Show();
        }

    }
}
