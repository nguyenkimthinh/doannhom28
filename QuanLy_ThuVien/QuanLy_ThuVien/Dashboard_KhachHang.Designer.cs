﻿namespace QuanLy_ThuVien
{
    partial class Dashboard_KhachHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_timMuonSach = new System.Windows.Forms.Button();
            this.button_XemTK = new System.Windows.Forms.Button();
            this.button_XemLichSuMuon = new System.Windows.Forms.Button();
            this.button_DangXuatKH = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label_Hello = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button_timMuonSach
            // 
            this.button_timMuonSach.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_timMuonSach.Location = new System.Drawing.Point(164, 80);
            this.button_timMuonSach.Name = "button_timMuonSach";
            this.button_timMuonSach.Size = new System.Drawing.Size(187, 38);
            this.button_timMuonSach.TabIndex = 0;
            this.button_timMuonSach.Text = "Tìm và mượn sách";
            this.button_timMuonSach.UseVisualStyleBackColor = true;
            // 
            // button_XemTK
            // 
            this.button_XemTK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_XemTK.Location = new System.Drawing.Point(164, 149);
            this.button_XemTK.Name = "button_XemTK";
            this.button_XemTK.Size = new System.Drawing.Size(187, 37);
            this.button_XemTK.TabIndex = 1;
            this.button_XemTK.Text = "Xem thông tin cá nhân";
            this.button_XemTK.UseVisualStyleBackColor = true;
            // 
            // button_XemLichSuMuon
            // 
            this.button_XemLichSuMuon.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_XemLichSuMuon.Location = new System.Drawing.Point(164, 212);
            this.button_XemLichSuMuon.Name = "button_XemLichSuMuon";
            this.button_XemLichSuMuon.Size = new System.Drawing.Size(187, 37);
            this.button_XemLichSuMuon.TabIndex = 2;
            this.button_XemLichSuMuon.Text = "Xem lịch sử mượn";
            this.button_XemLichSuMuon.UseVisualStyleBackColor = true;
            // 
            // button_DangXuatKH
            // 
            this.button_DangXuatKH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_DangXuatKH.Location = new System.Drawing.Point(164, 277);
            this.button_DangXuatKH.Name = "button_DangXuatKH";
            this.button_DangXuatKH.Size = new System.Drawing.Size(187, 36);
            this.button_DangXuatKH.TabIndex = 3;
            this.button_DangXuatKH.Text = "Đăng xuất";
            this.button_DangXuatKH.UseVisualStyleBackColor = true;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox4.Image = global::QuanLy_ThuVien.Properties.Resources._08442015014444dangnhap_dangxuat;
            this.pictureBox4.Location = new System.Drawing.Point(129, 276);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(39, 37);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 7;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox3.Image = global::QuanLy_ThuVien.Properties.Resources.icon_rut_ngan_thoi_gian;
            this.pictureBox3.Location = new System.Drawing.Point(129, 212);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(39, 37);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Image = global::QuanLy_ThuVien.Properties.Resources.icon_get_xem;
            this.pictureBox2.Location = new System.Drawing.Point(129, 149);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(39, 37);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = global::QuanLy_ThuVien.Properties.Resources.download1;
            this.pictureBox1.Location = new System.Drawing.Point(129, 80);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(39, 37);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // label_Hello
            // 
            this.label_Hello.AutoSize = true;
            this.label_Hello.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Hello.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label_Hello.Location = new System.Drawing.Point(105, 18);
            this.label_Hello.Name = "label_Hello";
            this.label_Hello.Size = new System.Drawing.Size(450, 24);
            this.label_Hello.TabIndex = 8;
            this.label_Hello.Text = "Hệ thống thư viện TWEight xin chào quý khách";
            // 
            // Dashboard_KhachHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 404);
            this.Controls.Add(this.label_Hello);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button_DangXuatKH);
            this.Controls.Add(this.button_XemLichSuMuon);
            this.Controls.Add(this.button_XemTK);
            this.Controls.Add(this.button_timMuonSach);
            this.Name = "Dashboard_KhachHang";
            this.Text = "Dashboard_KhachHang";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_timMuonSach;
        private System.Windows.Forms.Button button_XemTK;
        private System.Windows.Forms.Button button_XemLichSuMuon;
        private System.Windows.Forms.Button button_DangXuatKH;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label_Hello;
    }
}