﻿namespace QuanLy_ThuVien
{
    partial class Dashboard_Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_TimMuonSach = new System.Windows.Forms.Button();
            this.button_ChinhSuaSach = new System.Windows.Forms.Button();
            this.button_XemThongTin = new System.Windows.Forms.Button();
            this.button_TraSach = new System.Windows.Forms.Button();
            this.button_ThongKe = new System.Windows.Forms.Button();
            this.button_DangXuatAd = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button_TimMuonSach
            // 
            this.button_TimMuonSach.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_TimMuonSach.Location = new System.Drawing.Point(170, 65);
            this.button_TimMuonSach.Name = "button_TimMuonSach";
            this.button_TimMuonSach.Size = new System.Drawing.Size(192, 38);
            this.button_TimMuonSach.TabIndex = 0;
            this.button_TimMuonSach.Text = "Tìm và mượn sách";
            this.button_TimMuonSach.UseVisualStyleBackColor = true;
            // 
            // button_ChinhSuaSach
            // 
            this.button_ChinhSuaSach.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ChinhSuaSach.Location = new System.Drawing.Point(170, 123);
            this.button_ChinhSuaSach.Name = "button_ChinhSuaSach";
            this.button_ChinhSuaSach.Size = new System.Drawing.Size(192, 40);
            this.button_ChinhSuaSach.TabIndex = 1;
            this.button_ChinhSuaSach.Text = "Chỉnh sửa thông tin sách";
            this.button_ChinhSuaSach.UseVisualStyleBackColor = true;
            // 
            // button_XemThongTin
            // 
            this.button_XemThongTin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_XemThongTin.Location = new System.Drawing.Point(170, 186);
            this.button_XemThongTin.Name = "button_XemThongTin";
            this.button_XemThongTin.Size = new System.Drawing.Size(192, 38);
            this.button_XemThongTin.TabIndex = 2;
            this.button_XemThongTin.Text = "Xem thông tin";
            this.button_XemThongTin.UseVisualStyleBackColor = true;
            // 
            // button_TraSach
            // 
            this.button_TraSach.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_TraSach.Location = new System.Drawing.Point(170, 246);
            this.button_TraSach.Name = "button_TraSach";
            this.button_TraSach.Size = new System.Drawing.Size(192, 40);
            this.button_TraSach.TabIndex = 3;
            this.button_TraSach.Text = "Thông tin trả sách";
            this.button_TraSach.UseVisualStyleBackColor = true;
            // 
            // button_ThongKe
            // 
            this.button_ThongKe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ThongKe.Location = new System.Drawing.Point(170, 312);
            this.button_ThongKe.Name = "button_ThongKe";
            this.button_ThongKe.Size = new System.Drawing.Size(192, 36);
            this.button_ThongKe.TabIndex = 4;
            this.button_ThongKe.Text = "Thống kê";
            this.button_ThongKe.UseVisualStyleBackColor = true;
            // 
            // button_DangXuatAd
            // 
            this.button_DangXuatAd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_DangXuatAd.Location = new System.Drawing.Point(170, 373);
            this.button_DangXuatAd.Name = "button_DangXuatAd";
            this.button_DangXuatAd.Size = new System.Drawing.Size(192, 40);
            this.button_DangXuatAd.TabIndex = 5;
            this.button_DangXuatAd.Text = "Đăng xuất";
            this.button_DangXuatAd.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(47, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(590, 24);
            this.label1.TabIndex = 12;
            this.label1.Text = "Hệ thống thư viện TWEight chúc bạn một ngày làm việc vui vẻ";
            // 
            // pictureBox6
            // 
            this.pictureBox6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox6.Image = global::QuanLy_ThuVien.Properties.Resources._08442015014444dangnhap_dangxuat;
            this.pictureBox6.Location = new System.Drawing.Point(131, 373);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(42, 40);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 11;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox5.Image = global::QuanLy_ThuVien.Properties.Resources.pngtree_students_learn_hand_painted_picture_icon_statistics_tab_png_clipart_446763;
            this.pictureBox5.Location = new System.Drawing.Point(131, 308);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(42, 40);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 10;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox4.Image = global::QuanLy_ThuVien.Properties.Resources.icon_gia_re;
            this.pictureBox4.Location = new System.Drawing.Point(131, 246);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(42, 40);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 9;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox3.Image = global::QuanLy_ThuVien.Properties.Resources.icon_get_xem;
            this.pictureBox3.Location = new System.Drawing.Point(131, 184);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(42, 40);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 8;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Image = global::QuanLy_ThuVien.Properties.Resources.download__1_;
            this.pictureBox2.Location = new System.Drawing.Point(131, 123);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(42, 40);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = global::QuanLy_ThuVien.Properties.Resources.download1;
            this.pictureBox1.Location = new System.Drawing.Point(131, 65);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(42, 38);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // Dashboard_Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 462);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button_DangXuatAd);
            this.Controls.Add(this.button_ThongKe);
            this.Controls.Add(this.button_TraSach);
            this.Controls.Add(this.button_XemThongTin);
            this.Controls.Add(this.button_ChinhSuaSach);
            this.Controls.Add(this.button_TimMuonSach);
            this.Name = "Dashboard_Admin";
            this.Text = "Dashboard_Admin";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_TimMuonSach;
        private System.Windows.Forms.Button button_ChinhSuaSach;
        private System.Windows.Forms.Button button_XemThongTin;
        private System.Windows.Forms.Button button_TraSach;
        private System.Windows.Forms.Button button_ThongKe;
        private System.Windows.Forms.Button button_DangXuatAd;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label1;
    }
}