﻿namespace QuanLy_ThuVien
{
    partial class DangKi_KhachHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtGioiTinh = new System.Windows.Forms.TextBox();
            this.lbGioiTinh = new System.Windows.Forms.Label();
            this.btnDKTaiKhoan = new System.Windows.Forms.Button();
            this.txtXNMatKhau = new System.Windows.Forms.TextBox();
            this.txtMatKhau = new System.Windows.Forms.TextBox();
            this.txtTaiKhoan = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtHoTen = new System.Windows.Forms.TextBox();
            this.DKMatKhauAgain = new System.Windows.Forms.Label();
            this.DKMatKhau = new System.Windows.Forms.Label();
            this.DKTaiKhoan = new System.Windows.Forms.Label();
            this.DKSĐT = new System.Windows.Forms.Label();
            this.DKEmail = new System.Windows.Forms.Label();
            this.DKName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtGioiTinh
            // 
            this.txtGioiTinh.Location = new System.Drawing.Point(214, 103);
            this.txtGioiTinh.Name = "txtGioiTinh";
            this.txtGioiTinh.Size = new System.Drawing.Size(224, 20);
            this.txtGioiTinh.TabIndex = 29;
            // 
            // lbGioiTinh
            // 
            this.lbGioiTinh.AutoSize = true;
            this.lbGioiTinh.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGioiTinh.Location = new System.Drawing.Point(59, 103);
            this.lbGioiTinh.Name = "lbGioiTinh";
            this.lbGioiTinh.Size = new System.Drawing.Size(68, 16);
            this.lbGioiTinh.TabIndex = 28;
            this.lbGioiTinh.Text = "Giới tính:";
            // 
            // btnDKTaiKhoan
            // 
            this.btnDKTaiKhoan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnDKTaiKhoan.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDKTaiKhoan.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDKTaiKhoan.Location = new System.Drawing.Point(368, 393);
            this.btnDKTaiKhoan.Name = "btnDKTaiKhoan";
            this.btnDKTaiKhoan.Size = new System.Drawing.Size(89, 42);
            this.btnDKTaiKhoan.TabIndex = 27;
            this.btnDKTaiKhoan.Text = "Đăng kí";
            this.btnDKTaiKhoan.UseVisualStyleBackColor = false;
            this.btnDKTaiKhoan.Click += new System.EventHandler(this.btnDKTaiKhoan_Click);
            // 
            // txtXNMatKhau
            // 
            this.txtXNMatKhau.Location = new System.Drawing.Point(214, 326);
            this.txtXNMatKhau.Name = "txtXNMatKhau";
            this.txtXNMatKhau.Size = new System.Drawing.Size(224, 20);
            this.txtXNMatKhau.TabIndex = 26;
            // 
            // txtMatKhau
            // 
            this.txtMatKhau.Location = new System.Drawing.Point(214, 286);
            this.txtMatKhau.Name = "txtMatKhau";
            this.txtMatKhau.Size = new System.Drawing.Size(224, 20);
            this.txtMatKhau.TabIndex = 25;
            // 
            // txtTaiKhoan
            // 
            this.txtTaiKhoan.Location = new System.Drawing.Point(214, 243);
            this.txtTaiKhoan.Name = "txtTaiKhoan";
            this.txtTaiKhoan.Size = new System.Drawing.Size(224, 20);
            this.txtTaiKhoan.TabIndex = 24;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(214, 199);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(224, 20);
            this.txtPhone.TabIndex = 23;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(214, 153);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(224, 20);
            this.txtEmail.TabIndex = 22;
            // 
            // txtHoTen
            // 
            this.txtHoTen.Location = new System.Drawing.Point(214, 63);
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Size = new System.Drawing.Size(224, 20);
            this.txtHoTen.TabIndex = 21;
            // 
            // DKMatKhauAgain
            // 
            this.DKMatKhauAgain.AutoSize = true;
            this.DKMatKhauAgain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DKMatKhauAgain.Location = new System.Drawing.Point(59, 330);
            this.DKMatKhauAgain.Name = "DKMatKhauAgain";
            this.DKMatKhauAgain.Size = new System.Drawing.Size(136, 16);
            this.DKMatKhauAgain.TabIndex = 20;
            this.DKMatKhauAgain.Text = "Nhập lại mật khẩu:";
            // 
            // DKMatKhau
            // 
            this.DKMatKhau.AutoSize = true;
            this.DKMatKhau.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DKMatKhau.Location = new System.Drawing.Point(59, 286);
            this.DKMatKhau.Name = "DKMatKhau";
            this.DKMatKhau.Size = new System.Drawing.Size(74, 16);
            this.DKMatKhau.TabIndex = 19;
            this.DKMatKhau.Text = "Mật khẩu:";
            // 
            // DKTaiKhoan
            // 
            this.DKTaiKhoan.AutoSize = true;
            this.DKTaiKhoan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DKTaiKhoan.Location = new System.Drawing.Point(59, 243);
            this.DKTaiKhoan.Name = "DKTaiKhoan";
            this.DKTaiKhoan.Size = new System.Drawing.Size(82, 16);
            this.DKTaiKhoan.TabIndex = 18;
            this.DKTaiKhoan.Text = "Tài Khoản:";
            // 
            // DKSĐT
            // 
            this.DKSĐT.AutoSize = true;
            this.DKSĐT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DKSĐT.Location = new System.Drawing.Point(59, 199);
            this.DKSĐT.Name = "DKSĐT";
            this.DKSĐT.Size = new System.Drawing.Size(103, 16);
            this.DKSĐT.TabIndex = 17;
            this.DKSĐT.Text = "Số điện thoại:";
            // 
            // DKEmail
            // 
            this.DKEmail.AutoSize = true;
            this.DKEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DKEmail.Location = new System.Drawing.Point(59, 153);
            this.DKEmail.Name = "DKEmail";
            this.DKEmail.Size = new System.Drawing.Size(51, 16);
            this.DKEmail.TabIndex = 16;
            this.DKEmail.Text = "Email:";
            // 
            // DKName
            // 
            this.DKName.AutoSize = true;
            this.DKName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DKName.Location = new System.Drawing.Point(59, 63);
            this.DKName.Name = "DKName";
            this.DKName.Size = new System.Drawing.Size(63, 16);
            this.DKName.TabIndex = 15;
            this.DKName.Text = "Họ Tên:";
            // 
            // DangKi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 522);
            this.Controls.Add(this.txtGioiTinh);
            this.Controls.Add(this.lbGioiTinh);
            this.Controls.Add(this.btnDKTaiKhoan);
            this.Controls.Add(this.txtXNMatKhau);
            this.Controls.Add(this.txtMatKhau);
            this.Controls.Add(this.txtTaiKhoan);
            this.Controls.Add(this.txtPhone);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtHoTen);
            this.Controls.Add(this.DKMatKhauAgain);
            this.Controls.Add(this.DKMatKhau);
            this.Controls.Add(this.DKTaiKhoan);
            this.Controls.Add(this.DKSĐT);
            this.Controls.Add(this.DKEmail);
            this.Controls.Add(this.DKName);
            this.Name = "DangKi";
            this.Text = "DangKi";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtGioiTinh;
        private System.Windows.Forms.Label lbGioiTinh;
        private System.Windows.Forms.Button btnDKTaiKhoan;
        private System.Windows.Forms.TextBox txtXNMatKhau;
        private System.Windows.Forms.TextBox txtMatKhau;
        private System.Windows.Forms.TextBox txtTaiKhoan;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtHoTen;
        private System.Windows.Forms.Label DKMatKhauAgain;
        private System.Windows.Forms.Label DKMatKhau;
        private System.Windows.Forms.Label DKTaiKhoan;
        private System.Windows.Forms.Label DKSĐT;
        private System.Windows.Forms.Label DKEmail;
        private System.Windows.Forms.Label DKName;
    }
}