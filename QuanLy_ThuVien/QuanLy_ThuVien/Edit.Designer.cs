﻿namespace QuanLy_ThuVien
{
    partial class Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label maSachLabel;
            System.Windows.Forms.Label tenSachLabel;
            System.Windows.Forms.Label tacGiaLabel;
            System.Windows.Forms.Label theLoaiLabel;
            System.Windows.Forms.Label nhaXuatBanLabel;
            System.Windows.Forms.Label giaSachLabel;
            System.Windows.Forms.Label soLuongLabel;
            System.Windows.Forms.Label tinhTrangLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Edit));
            this.qLThuVienDataSet = new QuanLy_ThuVien.QLThuVienDataSet();
            this.sACHBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sACHTableAdapter = new QuanLy_ThuVien.QLThuVienDataSetTableAdapters.SACHTableAdapter();
            this.tableAdapterManager = new QuanLy_ThuVien.QLThuVienDataSetTableAdapters.TableAdapterManager();
            this.sACHBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.sACHBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.maSachText = new System.Windows.Forms.TextBox();
            this.tenSachText = new System.Windows.Forms.TextBox();
            this.tacGiaText = new System.Windows.Forms.TextBox();
            this.theLoaiText = new System.Windows.Forms.TextBox();
            this.nhaXuatBanText = new System.Windows.Forms.TextBox();
            this.giaSachText = new System.Windows.Forms.TextBox();
            this.soLuongText = new System.Windows.Forms.TextBox();
            this.tinhTrangText = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.IDNeedToEdit = new System.Windows.Forms.TextBox();
            maSachLabel = new System.Windows.Forms.Label();
            tenSachLabel = new System.Windows.Forms.Label();
            tacGiaLabel = new System.Windows.Forms.Label();
            theLoaiLabel = new System.Windows.Forms.Label();
            nhaXuatBanLabel = new System.Windows.Forms.Label();
            giaSachLabel = new System.Windows.Forms.Label();
            soLuongLabel = new System.Windows.Forms.Label();
            tinhTrangLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.qLThuVienDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sACHBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sACHBindingNavigator)).BeginInit();
            this.sACHBindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // maSachLabel
            // 
            maSachLabel.AutoSize = true;
            maSachLabel.Location = new System.Drawing.Point(182, 69);
            maSachLabel.Name = "maSachLabel";
            maSachLabel.Size = new System.Drawing.Size(50, 13);
            maSachLabel.TabIndex = 1;
            maSachLabel.Text = "Mã Sách";
            // 
            // tenSachLabel
            // 
            tenSachLabel.AutoSize = true;
            tenSachLabel.Location = new System.Drawing.Point(182, 95);
            tenSachLabel.Name = "tenSachLabel";
            tenSachLabel.Size = new System.Drawing.Size(54, 13);
            tenSachLabel.TabIndex = 3;
            tenSachLabel.Text = "Tên Sách";
            // 
            // tacGiaLabel
            // 
            tacGiaLabel.AutoSize = true;
            tacGiaLabel.Location = new System.Drawing.Point(182, 121);
            tacGiaLabel.Name = "tacGiaLabel";
            tacGiaLabel.Size = new System.Drawing.Size(45, 13);
            tacGiaLabel.TabIndex = 5;
            tacGiaLabel.Text = "Tác Giả";
            // 
            // theLoaiLabel
            // 
            theLoaiLabel.AutoSize = true;
            theLoaiLabel.Location = new System.Drawing.Point(182, 147);
            theLoaiLabel.Name = "theLoaiLabel";
            theLoaiLabel.Size = new System.Drawing.Size(49, 13);
            theLoaiLabel.TabIndex = 7;
            theLoaiLabel.Text = "Thể Loại";
            // 
            // nhaXuatBanLabel
            // 
            nhaXuatBanLabel.AutoSize = true;
            nhaXuatBanLabel.Location = new System.Drawing.Point(182, 173);
            nhaXuatBanLabel.Name = "nhaXuatBanLabel";
            nhaXuatBanLabel.Size = new System.Drawing.Size(74, 13);
            nhaXuatBanLabel.TabIndex = 9;
            nhaXuatBanLabel.Text = "Nhà Xuất Bản";
            // 
            // giaSachLabel
            // 
            giaSachLabel.AutoSize = true;
            giaSachLabel.Location = new System.Drawing.Point(182, 199);
            giaSachLabel.Name = "giaSachLabel";
            giaSachLabel.Size = new System.Drawing.Size(51, 13);
            giaSachLabel.TabIndex = 11;
            giaSachLabel.Text = "Giá Sách";
            // 
            // soLuongLabel
            // 
            soLuongLabel.AutoSize = true;
            soLuongLabel.Location = new System.Drawing.Point(182, 225);
            soLuongLabel.Name = "soLuongLabel";
            soLuongLabel.Size = new System.Drawing.Size(53, 13);
            soLuongLabel.TabIndex = 13;
            soLuongLabel.Text = "Số Lượng";
            // 
            // tinhTrangLabel
            // 
            tinhTrangLabel.AutoSize = true;
            tinhTrangLabel.Location = new System.Drawing.Point(182, 251);
            tinhTrangLabel.Name = "tinhTrangLabel";
            tinhTrangLabel.Size = new System.Drawing.Size(59, 13);
            tinhTrangLabel.TabIndex = 15;
            tinhTrangLabel.Text = "Tình Trạng";
            // 
            // qLThuVienDataSet
            // 
            this.qLThuVienDataSet.DataSetName = "QLThuVienDataSet";
            this.qLThuVienDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sACHBindingSource
            // 
            this.sACHBindingSource.DataMember = "SACH";
            this.sACHBindingSource.DataSource = this.qLThuVienDataSet;
            // 
            // sACHTableAdapter
            // 
            this.sACHTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACCOUNTTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.DOCGIATableAdapter = null;
            this.tableAdapterManager.PHIEUMUONTableAdapter = null;
            this.tableAdapterManager.PHIEUTRATableAdapter = null;
            this.tableAdapterManager.SACHTableAdapter = this.sACHTableAdapter;
            this.tableAdapterManager.UpdateOrder = QuanLy_ThuVien.QLThuVienDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // sACHBindingNavigator
            // 
            this.sACHBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.sACHBindingNavigator.BindingSource = this.sACHBindingSource;
            this.sACHBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.sACHBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.sACHBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.sACHBindingNavigatorSaveItem});
            this.sACHBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.sACHBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.sACHBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.sACHBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.sACHBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.sACHBindingNavigator.Name = "sACHBindingNavigator";
            this.sACHBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.sACHBindingNavigator.Size = new System.Drawing.Size(595, 25);
            this.sACHBindingNavigator.TabIndex = 0;
            this.sACHBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // sACHBindingNavigatorSaveItem
            // 
            this.sACHBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.sACHBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("sACHBindingNavigatorSaveItem.Image")));
            this.sACHBindingNavigatorSaveItem.Name = "sACHBindingNavigatorSaveItem";
            this.sACHBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.sACHBindingNavigatorSaveItem.Text = "Save Data";
            this.sACHBindingNavigatorSaveItem.Click += new System.EventHandler(this.sACHBindingNavigatorSaveItem_Click);
            // 
            // maSachText
            // 
            this.maSachText.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sACHBindingSource, "MaSach", true));
            this.maSachText.Location = new System.Drawing.Point(265, 66);
            this.maSachText.Name = "maSachText";
            this.maSachText.Size = new System.Drawing.Size(165, 20);
            this.maSachText.TabIndex = 2;
            // 
            // tenSachText
            // 
            this.tenSachText.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sACHBindingSource, "TenSach", true));
            this.tenSachText.Location = new System.Drawing.Point(265, 92);
            this.tenSachText.Name = "tenSachText";
            this.tenSachText.Size = new System.Drawing.Size(165, 20);
            this.tenSachText.TabIndex = 4;
            // 
            // tacGiaText
            // 
            this.tacGiaText.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sACHBindingSource, "TacGia", true));
            this.tacGiaText.Location = new System.Drawing.Point(265, 118);
            this.tacGiaText.Name = "tacGiaText";
            this.tacGiaText.Size = new System.Drawing.Size(165, 20);
            this.tacGiaText.TabIndex = 6;
            // 
            // theLoaiText
            // 
            this.theLoaiText.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sACHBindingSource, "TheLoai", true));
            this.theLoaiText.Location = new System.Drawing.Point(265, 144);
            this.theLoaiText.Name = "theLoaiText";
            this.theLoaiText.Size = new System.Drawing.Size(165, 20);
            this.theLoaiText.TabIndex = 8;
            // 
            // nhaXuatBanText
            // 
            this.nhaXuatBanText.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sACHBindingSource, "NhaXuatBan", true));
            this.nhaXuatBanText.Location = new System.Drawing.Point(265, 170);
            this.nhaXuatBanText.Name = "nhaXuatBanText";
            this.nhaXuatBanText.Size = new System.Drawing.Size(165, 20);
            this.nhaXuatBanText.TabIndex = 10;
            // 
            // giaSachText
            // 
            this.giaSachText.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sACHBindingSource, "GiaSach", true));
            this.giaSachText.Location = new System.Drawing.Point(265, 196);
            this.giaSachText.Name = "giaSachText";
            this.giaSachText.Size = new System.Drawing.Size(165, 20);
            this.giaSachText.TabIndex = 12;
            // 
            // soLuongText
            // 
            this.soLuongText.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sACHBindingSource, "SoLuong", true));
            this.soLuongText.Location = new System.Drawing.Point(265, 222);
            this.soLuongText.Name = "soLuongText";
            this.soLuongText.Size = new System.Drawing.Size(165, 20);
            this.soLuongText.TabIndex = 14;
            // 
            // tinhTrangText
            // 
            this.tinhTrangText.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sACHBindingSource, "TinhTrang", true));
            this.tinhTrangText.Location = new System.Drawing.Point(265, 248);
            this.tinhTrangText.Name = "tinhTrangText";
            this.tinhTrangText.Size = new System.Drawing.Size(165, 20);
            this.tinhTrangText.TabIndex = 16;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(276, 304);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "Chỉnh sửa";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.EditBttClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(138, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Mã sách cần chỉnh sửa";
            // 
            // IDNeedToEdit
            // 
            this.IDNeedToEdit.Location = new System.Drawing.Point(265, 25);
            this.IDNeedToEdit.Name = "IDNeedToEdit";
            this.IDNeedToEdit.Size = new System.Drawing.Size(165, 20);
            this.IDNeedToEdit.TabIndex = 19;
            // 
            // Edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 417);
            this.Controls.Add(this.IDNeedToEdit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(maSachLabel);
            this.Controls.Add(this.maSachText);
            this.Controls.Add(tenSachLabel);
            this.Controls.Add(this.tenSachText);
            this.Controls.Add(tacGiaLabel);
            this.Controls.Add(this.tacGiaText);
            this.Controls.Add(theLoaiLabel);
            this.Controls.Add(this.theLoaiText);
            this.Controls.Add(nhaXuatBanLabel);
            this.Controls.Add(this.nhaXuatBanText);
            this.Controls.Add(giaSachLabel);
            this.Controls.Add(this.giaSachText);
            this.Controls.Add(soLuongLabel);
            this.Controls.Add(this.soLuongText);
            this.Controls.Add(tinhTrangLabel);
            this.Controls.Add(this.tinhTrangText);
            this.Controls.Add(this.sACHBindingNavigator);
            this.Name = "Edit";
            this.Text = "Edit";
            this.Load += new System.EventHandler(this.Edit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.qLThuVienDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sACHBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sACHBindingNavigator)).EndInit();
            this.sACHBindingNavigator.ResumeLayout(false);
            this.sACHBindingNavigator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private QLThuVienDataSet qLThuVienDataSet;
        private System.Windows.Forms.BindingSource sACHBindingSource;
        private QLThuVienDataSetTableAdapters.SACHTableAdapter sACHTableAdapter;
        private QLThuVienDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator sACHBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton sACHBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox maSachText;
        private System.Windows.Forms.TextBox tenSachText;
        private System.Windows.Forms.TextBox tacGiaText;
        private System.Windows.Forms.TextBox theLoaiText;
        private System.Windows.Forms.TextBox nhaXuatBanText;
        private System.Windows.Forms.TextBox giaSachText;
        private System.Windows.Forms.TextBox soLuongText;
        private System.Windows.Forms.TextBox tinhTrangText;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox IDNeedToEdit;
    }
}