﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLy_ThuVien
{
    public partial class Intro : Form
    {
        DangNhap_KhachHang KH;
        DangNhap_Admin Ad;
        public Intro()
        {
            InitializeComponent();
        }

        private void button_KhachHang_Click(object sender, EventArgs e)
        {
            this.Hide();
            KH = new DangNhap_KhachHang();
            KH.Show();
        }

        private void button_Admin_Click(object sender, EventArgs e)
        {
            this.Hide();
            Ad = new DangNhap_Admin();
            Ad.Show();
        }
    }
}
