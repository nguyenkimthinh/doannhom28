﻿namespace QuanLy_ThuVien
{
    partial class DangNhap_Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDangKi_Admin = new System.Windows.Forms.Label();
            this.labInCorrect = new System.Windows.Forms.Label();
            this.Cancel_Admin = new System.Windows.Forms.Button();
            this.Login_Admind = new System.Windows.Forms.Button();
            this.checkHienThiPass = new System.Windows.Forms.CheckBox();
            this.labPassword = new System.Windows.Forms.Label();
            this.txtPassword_Admin = new System.Windows.Forms.TextBox();
            this.txtUserName_Admin = new System.Windows.Forms.TextBox();
            this.labUserName = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.PicPassword = new System.Windows.Forms.PictureBox();
            this.PicUserName = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicUserName)).BeginInit();
            this.SuspendLayout();
            // 
            // txtDangKi_Admin
            // 
            this.txtDangKi_Admin.AutoSize = true;
            this.txtDangKi_Admin.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDangKi_Admin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtDangKi_Admin.Location = new System.Drawing.Point(40, 276);
            this.txtDangKi_Admin.Name = "txtDangKi_Admin";
            this.txtDangKi_Admin.Size = new System.Drawing.Size(99, 19);
            this.txtDangKi_Admin.TabIndex = 35;
            this.txtDangKi_Admin.Text = "Đăng kí mới?";
            this.txtDangKi_Admin.Click += new System.EventHandler(this.txtDangKi_Admin_Click);
            // 
            // labInCorrect
            // 
            this.labInCorrect.AutoSize = true;
            this.labInCorrect.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labInCorrect.ForeColor = System.Drawing.Color.Red;
            this.labInCorrect.Location = new System.Drawing.Point(76, 239);
            this.labInCorrect.Name = "labInCorrect";
            this.labInCorrect.Size = new System.Drawing.Size(0, 19);
            this.labInCorrect.TabIndex = 31;
            // 
            // Cancel_Admin
            // 
            this.Cancel_Admin.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancel_Admin.Location = new System.Drawing.Point(360, 291);
            this.Cancel_Admin.Name = "Cancel_Admin";
            this.Cancel_Admin.Size = new System.Drawing.Size(75, 33);
            this.Cancel_Admin.TabIndex = 30;
            this.Cancel_Admin.Text = "Cancel";
            this.Cancel_Admin.UseVisualStyleBackColor = true;
            this.Cancel_Admin.Click += new System.EventHandler(this.Cancel_Admin_Click);
            // 
            // Login_Admind
            // 
            this.Login_Admind.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Login_Admind.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Login_Admind.ForeColor = System.Drawing.Color.Black;
            this.Login_Admind.Location = new System.Drawing.Point(235, 291);
            this.Login_Admind.Name = "Login_Admind";
            this.Login_Admind.Size = new System.Drawing.Size(75, 33);
            this.Login_Admind.TabIndex = 29;
            this.Login_Admind.Text = "Login";
            this.Login_Admind.UseVisualStyleBackColor = false;
            this.Login_Admind.Click += new System.EventHandler(this.Login_Admind_Click);
            // 
            // checkHienThiPass
            // 
            this.checkHienThiPass.AutoSize = true;
            this.checkHienThiPass.Location = new System.Drawing.Point(208, 176);
            this.checkHienThiPass.Name = "checkHienThiPass";
            this.checkHienThiPass.Size = new System.Drawing.Size(102, 17);
            this.checkHienThiPass.TabIndex = 28;
            this.checkHienThiPass.Text = "Show Password";
            this.checkHienThiPass.UseVisualStyleBackColor = true;
            this.checkHienThiPass.CheckedChanged += new System.EventHandler(this.checkHienThiPass_CheckedChanged);
            // 
            // labPassword
            // 
            this.labPassword.AutoSize = true;
            this.labPassword.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.labPassword.Location = new System.Drawing.Point(162, 106);
            this.labPassword.Name = "labPassword";
            this.labPassword.Size = new System.Drawing.Size(90, 22);
            this.labPassword.TabIndex = 27;
            this.labPassword.Text = "Password";
            // 
            // txtPassword_Admin
            // 
            this.txtPassword_Admin.Location = new System.Drawing.Point(239, 135);
            this.txtPassword_Admin.Name = "txtPassword_Admin";
            this.txtPassword_Admin.Size = new System.Drawing.Size(181, 20);
            this.txtPassword_Admin.TabIndex = 26;
            this.txtPassword_Admin.UseSystemPasswordChar = true;
            this.txtPassword_Admin.TextChanged += new System.EventHandler(this.txtPassword_Admin_TextChanged);
            // 
            // txtUserName_Admin
            // 
            this.txtUserName_Admin.Location = new System.Drawing.Point(239, 83);
            this.txtUserName_Admin.Name = "txtUserName_Admin";
            this.txtUserName_Admin.Size = new System.Drawing.Size(181, 20);
            this.txtUserName_Admin.TabIndex = 25;
            this.txtUserName_Admin.TextChanged += new System.EventHandler(this.txtUserName_Admin_TextChanged);
            // 
            // labUserName
            // 
            this.labUserName.AutoSize = true;
            this.labUserName.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.labUserName.Location = new System.Drawing.Point(162, 38);
            this.labUserName.Name = "labUserName";
            this.labUserName.Size = new System.Drawing.Size(96, 22);
            this.labUserName.TabIndex = 24;
            this.labUserName.Text = "UserName";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = global::QuanLy_ThuVien.Properties.Resources.download;
            this.pictureBox1.Location = new System.Drawing.Point(6, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 168);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 34;
            this.pictureBox1.TabStop = false;
            // 
            // PicPassword
            // 
            this.PicPassword.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PicPassword.Image = global::QuanLy_ThuVien.Properties.Resources.pass;
            this.PicPassword.Location = new System.Drawing.Point(208, 131);
            this.PicPassword.Name = "PicPassword";
            this.PicPassword.Size = new System.Drawing.Size(32, 24);
            this.PicPassword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicPassword.TabIndex = 33;
            this.PicPassword.TabStop = false;
            // 
            // PicUserName
            // 
            this.PicUserName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PicUserName.Image = global::QuanLy_ThuVien.Properties.Resources.depositphotos_182720668_stock_illustration_mail_envelope_icon_symbol_of;
            this.PicUserName.Location = new System.Drawing.Point(208, 71);
            this.PicUserName.Name = "PicUserName";
            this.PicUserName.Size = new System.Drawing.Size(33, 32);
            this.PicUserName.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicUserName.TabIndex = 32;
            this.PicUserName.TabStop = false;
            // 
            // DangNhap_Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 346);
            this.Controls.Add(this.txtDangKi_Admin);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.PicPassword);
            this.Controls.Add(this.PicUserName);
            this.Controls.Add(this.labInCorrect);
            this.Controls.Add(this.Cancel_Admin);
            this.Controls.Add(this.Login_Admind);
            this.Controls.Add(this.checkHienThiPass);
            this.Controls.Add(this.labPassword);
            this.Controls.Add(this.txtPassword_Admin);
            this.Controls.Add(this.txtUserName_Admin);
            this.Controls.Add(this.labUserName);
            this.Name = "DangNhap_Admin";
            this.Text = "DangNhap_Admin";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicUserName)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label txtDangKi_Admin;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox PicPassword;
        private System.Windows.Forms.PictureBox PicUserName;
        private System.Windows.Forms.Label labInCorrect;
        private System.Windows.Forms.Button Cancel_Admin;
        private System.Windows.Forms.Button Login_Admind;
        private System.Windows.Forms.CheckBox checkHienThiPass;
        private System.Windows.Forms.Label labPassword;
        private System.Windows.Forms.TextBox txtPassword_Admin;
        private System.Windows.Forms.TextBox txtUserName_Admin;
        private System.Windows.Forms.Label labUserName;
    }
}