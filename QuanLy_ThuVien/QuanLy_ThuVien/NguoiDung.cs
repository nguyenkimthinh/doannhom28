﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLy_ThuVien
{
    class NguoiDung
    {
        private string HoTen, GioiTinh, Email, Phone, Taikhoan, Matkhau;
        public NguoiDung()
        {
            HoTen = Email = GioiTinh= Phone = Taikhoan = Matkhau = "";
        }
        public NguoiDung(string ten, string gioitinh, string mail, string sdt, string tk, string mk)
        {
            HoTen = ten;
            GioiTinh = gioitinh;
            Email = mail;
            Phone = sdt;
            Taikhoan = tk;
            Matkhau = mk;

        }
        public bool KiemTraDinhDangMatKhau()
        {
            bool kiemtradodai = false;

            if (Matkhau.Length >= 8)
            {
                kiemtradodai = true;
            }
            bool kiemtrachu = false;
            bool kiemtraso = false;
            for (int i = 0; i < Matkhau.Length; ++i)
            {
                if (kiemtraso == true && kiemtrachu == true)
                {
                    break;
                }

                if ((Matkhau[i] >= 'A' && Matkhau[i] <= 'Z') || (Matkhau[i] >= 'a' && Matkhau[i] <= 'z'))
                {
                    kiemtrachu = true;
                }
                if (Matkhau[i] >= '0' && Matkhau[i] <= '9')
                {
                    kiemtraso = true;
                }
            }
            if (kiemtraso == true && kiemtrachu == true && kiemtradodai == true)
            {
                return true;
            }
            return false;
        }
    }
}
