﻿namespace fTimSach
{
    partial class fXemHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.txbMadocgia = new System.Windows.Forms.TextBox();
            this.lb1 = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.tbLichsumuon = new System.Windows.Forms.TabControl();
            this.tpDangmuon = new System.Windows.Forms.TabPage();
            this.dtgvDangmuon = new System.Windows.Forms.DataGridView();
            this.tpSachdatra = new System.Windows.Forms.TabPage();
            this.dtgvDatra = new System.Windows.Forms.DataGridView();
            this.btnXem = new System.Windows.Forms.Button();
            this.tcbView = new System.Windows.Forms.TabControl();
            this.tcbView1user = new System.Windows.Forms.TabPage();
            this.tcbViewNuser = new System.Windows.Forms.TabPage();
            this.btnView = new System.Windows.Forms.Button();
            this.Tab2 = new System.Windows.Forms.TabControl();
            this.tbViewMuon = new System.Windows.Forms.TabPage();
            this.dtgvListMuon = new System.Windows.Forms.DataGridView();
            this.tbViewTra = new System.Windows.Forms.TabPage();
            this.dtgvListTra = new System.Windows.Forms.DataGridView();
            this.txbMaUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.tbLichsumuon.SuspendLayout();
            this.tpDangmuon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvDangmuon)).BeginInit();
            this.tpSachdatra.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvDatra)).BeginInit();
            this.tcbView.SuspendLayout();
            this.tcbView1user.SuspendLayout();
            this.tcbViewNuser.SuspendLayout();
            this.Tab2.SuspendLayout();
            this.tbViewMuon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvListMuon)).BeginInit();
            this.tbViewTra.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvListTra)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txbMadocgia);
            this.panel2.Controls.Add(this.lb1);
            this.panel2.Location = new System.Drawing.Point(305, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(244, 37);
            this.panel2.TabIndex = 12;
            // 
            // txbMadocgia
            // 
            this.txbMadocgia.Location = new System.Drawing.Point(134, 8);
            this.txbMadocgia.Name = "txbMadocgia";
            this.txbMadocgia.Size = new System.Drawing.Size(104, 20);
            this.txbMadocgia.TabIndex = 2;
            // 
            // lb1
            // 
            this.lb1.AutoSize = true;
            this.lb1.Location = new System.Drawing.Point(45, 11);
            this.lb1.Name = "lb1";
            this.lb1.Size = new System.Drawing.Size(83, 13);
            this.lb1.TabIndex = 1;
            this.lb1.Text = "Mã Khách hàng";
            // 
            // btnBack
            // 
            this.btnBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBack.Location = new System.Drawing.Point(13, 17);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 14;
            this.btnBack.Text = "Trở về";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // tbLichsumuon
            // 
            this.tbLichsumuon.Controls.Add(this.tpDangmuon);
            this.tbLichsumuon.Controls.Add(this.tpSachdatra);
            this.tbLichsumuon.Location = new System.Drawing.Point(6, 37);
            this.tbLichsumuon.Name = "tbLichsumuon";
            this.tbLichsumuon.SelectedIndex = 0;
            this.tbLichsumuon.Size = new System.Drawing.Size(632, 324);
            this.tbLichsumuon.TabIndex = 15;
            // 
            // tpDangmuon
            // 
            this.tpDangmuon.Controls.Add(this.dtgvDangmuon);
            this.tpDangmuon.Location = new System.Drawing.Point(4, 22);
            this.tpDangmuon.Name = "tpDangmuon";
            this.tpDangmuon.Padding = new System.Windows.Forms.Padding(3);
            this.tpDangmuon.Size = new System.Drawing.Size(624, 298);
            this.tpDangmuon.TabIndex = 0;
            this.tpDangmuon.Text = "Sách đang mượn";
            this.tpDangmuon.UseVisualStyleBackColor = true;
            // 
            // dtgvDangmuon
            // 
            this.dtgvDangmuon.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvDangmuon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvDangmuon.Location = new System.Drawing.Point(7, 6);
            this.dtgvDangmuon.Name = "dtgvDangmuon";
            this.dtgvDangmuon.ReadOnly = true;
            this.dtgvDangmuon.Size = new System.Drawing.Size(607, 286);
            this.dtgvDangmuon.TabIndex = 0;
            // 
            // tpSachdatra
            // 
            this.tpSachdatra.Controls.Add(this.dtgvDatra);
            this.tpSachdatra.Location = new System.Drawing.Point(4, 22);
            this.tpSachdatra.Name = "tpSachdatra";
            this.tpSachdatra.Padding = new System.Windows.Forms.Padding(3);
            this.tpSachdatra.Size = new System.Drawing.Size(624, 298);
            this.tpSachdatra.TabIndex = 1;
            this.tpSachdatra.Text = "Sách đã trả";
            this.tpSachdatra.UseVisualStyleBackColor = true;
            // 
            // dtgvDatra
            // 
            this.dtgvDatra.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvDatra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvDatra.Location = new System.Drawing.Point(7, 20);
            this.dtgvDatra.Name = "dtgvDatra";
            this.dtgvDatra.ReadOnly = true;
            this.dtgvDatra.Size = new System.Drawing.Size(603, 280);
            this.dtgvDatra.TabIndex = 1;
            // 
            // btnXem
            // 
            this.btnXem.Location = new System.Drawing.Point(559, 12);
            this.btnXem.Name = "btnXem";
            this.btnXem.Size = new System.Drawing.Size(75, 23);
            this.btnXem.TabIndex = 16;
            this.btnXem.Text = "Xem";
            this.btnXem.UseVisualStyleBackColor = true;
            this.btnXem.Click += new System.EventHandler(this.btnXem_Click);
            // 
            // tcbView
            // 
            this.tcbView.Controls.Add(this.tcbView1user);
            this.tcbView.Controls.Add(this.tcbViewNuser);
            this.tcbView.Location = new System.Drawing.Point(13, 46);
            this.tcbView.Name = "tcbView";
            this.tcbView.SelectedIndex = 0;
            this.tcbView.Size = new System.Drawing.Size(652, 393);
            this.tcbView.TabIndex = 17;
            // 
            // tcbView1user
            // 
            this.tcbView1user.Controls.Add(this.tbLichsumuon);
            this.tcbView1user.Controls.Add(this.btnXem);
            this.tcbView1user.Controls.Add(this.panel2);
            this.tcbView1user.Location = new System.Drawing.Point(4, 22);
            this.tcbView1user.Name = "tcbView1user";
            this.tcbView1user.Padding = new System.Windows.Forms.Padding(3);
            this.tcbView1user.Size = new System.Drawing.Size(644, 367);
            this.tcbView1user.TabIndex = 0;
            this.tcbView1user.Text = "Xem theo mã KH";
            this.tcbView1user.UseVisualStyleBackColor = true;
            // 
            // tcbViewNuser
            // 
            this.tcbViewNuser.Controls.Add(this.btnView);
            this.tcbViewNuser.Controls.Add(this.Tab2);
            this.tcbViewNuser.Location = new System.Drawing.Point(4, 22);
            this.tcbViewNuser.Name = "tcbViewNuser";
            this.tcbViewNuser.Padding = new System.Windows.Forms.Padding(3);
            this.tcbViewNuser.Size = new System.Drawing.Size(644, 367);
            this.tcbViewNuser.TabIndex = 1;
            this.tcbViewNuser.Text = "Xem tất cả";
            this.tcbViewNuser.UseVisualStyleBackColor = true;
            // 
            // btnView
            // 
            this.btnView.Location = new System.Drawing.Point(556, 8);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(75, 23);
            this.btnView.TabIndex = 1;
            this.btnView.Text = "Xem";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // Tab2
            // 
            this.Tab2.Controls.Add(this.tbViewMuon);
            this.Tab2.Controls.Add(this.tbViewTra);
            this.Tab2.Location = new System.Drawing.Point(4, 37);
            this.Tab2.Name = "Tab2";
            this.Tab2.SelectedIndex = 0;
            this.Tab2.Size = new System.Drawing.Size(634, 324);
            this.Tab2.TabIndex = 0;
            // 
            // tbViewMuon
            // 
            this.tbViewMuon.Controls.Add(this.dtgvListMuon);
            this.tbViewMuon.Location = new System.Drawing.Point(4, 22);
            this.tbViewMuon.Name = "tbViewMuon";
            this.tbViewMuon.Padding = new System.Windows.Forms.Padding(3);
            this.tbViewMuon.Size = new System.Drawing.Size(626, 298);
            this.tbViewMuon.TabIndex = 0;
            this.tbViewMuon.Text = "Lịch sử mượn";
            this.tbViewMuon.UseVisualStyleBackColor = true;
            // 
            // dtgvListMuon
            // 
            this.dtgvListMuon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvListMuon.Location = new System.Drawing.Point(7, 17);
            this.dtgvListMuon.Name = "dtgvListMuon";
            this.dtgvListMuon.Size = new System.Drawing.Size(613, 275);
            this.dtgvListMuon.TabIndex = 0;
            // 
            // tbViewTra
            // 
            this.tbViewTra.Controls.Add(this.dtgvListTra);
            this.tbViewTra.Location = new System.Drawing.Point(4, 22);
            this.tbViewTra.Name = "tbViewTra";
            this.tbViewTra.Padding = new System.Windows.Forms.Padding(3);
            this.tbViewTra.Size = new System.Drawing.Size(626, 298);
            this.tbViewTra.TabIndex = 1;
            this.tbViewTra.Text = "Lịch sử trả";
            this.tbViewTra.UseVisualStyleBackColor = true;
            // 
            // dtgvListTra
            // 
            this.dtgvListTra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvListTra.Location = new System.Drawing.Point(4, 20);
            this.dtgvListTra.Name = "dtgvListTra";
            this.dtgvListTra.Size = new System.Drawing.Size(619, 275);
            this.dtgvListTra.TabIndex = 0;
            // 
            // txbMaUser
            // 
            this.txbMaUser.Location = new System.Drawing.Point(565, 29);
            this.txbMaUser.Name = "txbMaUser";
            this.txbMaUser.ReadOnly = true;
            this.txbMaUser.Size = new System.Drawing.Size(100, 20);
            this.txbMaUser.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(573, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Mã người dùng";
            // 
            // fXemHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnBack;
            this.ClientSize = new System.Drawing.Size(673, 441);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txbMaUser);
            this.Controls.Add(this.tcbView);
            this.Controls.Add(this.btnBack);
            this.Name = "fXemHistory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lịch sử mượn";
            this.Load += new System.EventHandler(this.fXemHistory_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tbLichsumuon.ResumeLayout(false);
            this.tpDangmuon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvDangmuon)).EndInit();
            this.tpSachdatra.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvDatra)).EndInit();
            this.tcbView.ResumeLayout(false);
            this.tcbView1user.ResumeLayout(false);
            this.tcbViewNuser.ResumeLayout(false);
            this.Tab2.ResumeLayout(false);
            this.tbViewMuon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvListMuon)).EndInit();
            this.tbViewTra.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvListTra)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lb1;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.TabControl tbLichsumuon;
        private System.Windows.Forms.TabPage tpDangmuon;
        private System.Windows.Forms.DataGridView dtgvDangmuon;
        private System.Windows.Forms.TabPage tpSachdatra;
        private System.Windows.Forms.DataGridView dtgvDatra;
        private System.Windows.Forms.TextBox txbMadocgia;
        private System.Windows.Forms.Button btnXem;
        private System.Windows.Forms.TabControl tcbView;
        private System.Windows.Forms.TabPage tcbView1user;
        private System.Windows.Forms.TabPage tcbViewNuser;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.TabControl Tab2;
        private System.Windows.Forms.TabPage tbViewMuon;
        private System.Windows.Forms.DataGridView dtgvListMuon;
        private System.Windows.Forms.TabPage tbViewTra;
        private System.Windows.Forms.DataGridView dtgvListTra;
        private System.Windows.Forms.TextBox txbMaUser;
        private System.Windows.Forms.Label label1;
    }
}