﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using fTimSach.DAO;

namespace fTimSach
{
    public partial class fXemHistory : Form
    {
        
        public fXemHistory()
        {
            InitializeComponent();
        }
        public void funData(string ID)
        {
            txbMaUser.Text = ID ;
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        void LoadSachdangmuon(string madg)
        {
            string query = string.Format("SELECT TEMPT1.MaKH, TEMPT1.MaPhieu, TEMPT1.MaSach, s.TenSach, TEMPT1.NgayMuon, TEMPT1.NgayPhaiTra FROM (SELECT pm.MaKH, pm.MaPhieu, pm.MaSach, pm.NgayMuon, pm.NgayPhaiTra FROM (SELECT pm.MaPhieu,pm.MaKH,pm.MaSach FROM PhieuMuon pm WHERE pm.MaKH = {0} EXCEPT SELECT pt.MaPhieu, pt.MaKH, pt.MaSach FROM PhieuTra pt WHERE pt.MaKH = {0}) TEMPT0, PhieuMuon pm WHERE pm.MaKH = TEMPT0.MaKH AND pm.MaPhieu = TEMPT0.MaPhieu AND pm.MaSach = TEMPT0.MaSach) AS TEMPT1, Sach s WHERE s.MaSach = TEMPT1.MaSach ORDER BY NgayMuon DESC", madg);
            dtgvDangmuon.DataSource = DataProvider.Instance.ExecuteQuery(query);
            dtgvDangmuon.Columns[4].DefaultCellStyle.Format = "dd/MM/yyyy";
            dtgvDangmuon.Columns[5].DefaultCellStyle.Format = "dd/MM/yyyy";
            dtgvDangmuon.Columns[0].HeaderText = "Mã KH";
            dtgvDangmuon.Columns[1].HeaderText = "Mã Phiếu";
            dtgvDangmuon.Columns[2].HeaderText = "Mã sách";
            dtgvDangmuon.Columns[3].HeaderText = "Tên sách";
            dtgvDangmuon.Columns[4].HeaderText = "Ngày mượn";
            dtgvDangmuon.Columns[5].HeaderText = "Ngày phải trả";
        }
        void LoadDatra(string madg)
        {
            string query = string.Format("SELECT pt.MaKH, pt.MaPhieu, pt.MaSach, s.TenSach, pt.NgayTra, pt.TongTien FROM PhieuTra pt, Sach s WHERE (pt.MaKH = {0}) AND (s.MaSach = pt.MaSach) ORDER BY pt.NgayTra DESC", madg);
            dtgvDatra.DataSource = DataProvider.Instance.ExecuteQuery(query);
            
            dtgvDatra.Columns[4].DefaultCellStyle.Format = "dd/MM/yyyy";
            dtgvDatra.Columns[0].HeaderText = "Mã KH";
            dtgvDatra.Columns[1].HeaderText = "Mã phiếu";
            dtgvDatra.Columns[2].HeaderText = "Mã sách";
            dtgvDatra.Columns[3].HeaderText = "Tên sách";
            dtgvDatra.Columns[4].HeaderText = "Ngày trả";
            dtgvDatra.Columns[5].HeaderText = "Phí mượn";
        }

        private void btnXem_Click(object sender, EventArgs e)
        {
            if (txbMadocgia.Text != "")
            {
                string MaKH = txbMadocgia.Text.ToString();
                int i;

                if (!Int32.TryParse(MaKH, out i))
                {
                    i = -1;
                }
                if (i == -1)
                {
                    MessageBox.Show("Mã khách hàng không tồn tại.");
                }
                else
                {
                    DataTable data = DataProvider.Instance.ExecuteQuery(string.Format("SELECT Hoten, MaKH FROM KhachHang WHERE MaKH = {0}", MaKH));
                    if (data.Rows.Count > 0)
                    {
                        LoadSachdangmuon(txbMadocgia.Text);
                        LoadDatra(txbMadocgia.Text);
                    }
                    else
                        MessageBox.Show("Mã khách hàng không tồn tại.");
                }
               
            }
            else
                MessageBox.Show("Chưa nhập mã khách hàng", "Thông báo");
           
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            dtgvListMuon.DataSource = DataProvider.Instance.ExecuteQuery("SELECT MaPhieu as N'Mã Phiếu', MaKH as N'Mã KH', s.MaSach as N'Mã sách', TenSach as N'Tên sách', NgayMuon as N'Ngày mượn', NgayPhaiTra as N'Ngày phải trả' FROM PhieuMuon pm, Sach s WHERE s.MaSach = pm.MaSach ORDER BY NgayMuon DESC");
            dtgvListTra.DataSource = DataProvider.Instance.ExecuteQuery("SELECT MaPhieu as N'Mã Phiếu', MaKH as N'Mã KH', s.MaSach as N'Mã sách', TenSach  as N'Tên sách', NgayTra as N'Ngày trả' FROM PhieuTra pt, Sach s WHERE s.MaSach = pt.MaSach ORDER BY NgayTra DESC");
            dtgvListMuon.Columns[4].DefaultCellStyle.Format = "dd/MM/yyyy";
            dtgvListMuon.Columns[5].DefaultCellStyle.Format = "dd/MM/yyyy";
            dtgvListTra.Columns[4].DefaultCellStyle.Format = "dd/MM/yyyy";
        }

        private void fXemHistory_Load(object sender, EventArgs e)
        {
            string query = "SELECT PhanQuyen FROM TaiKhoan tk WHERE tk.MaKH = " + txbMaUser.Text.ToString();
            DataTable dt = DataProvider.Instance.ExecuteQuery(query);
            if (dt.Rows[0][0].ToString() == "0")
            {
                // Quan li
                //MessageBox.Show("Bạn là quản lí");
                
            }
            else
            {
                // khach hang
                // MessageBox.Show("Bạn là khách hàng");
                //this.Tab2.Enabled = false;
                txbMadocgia.Text = txbMaUser.Text;
                txbMadocgia.ReadOnly = true;
                tcbViewNuser.Enabled = false;
            }
        }

      

       
            
    }
}
