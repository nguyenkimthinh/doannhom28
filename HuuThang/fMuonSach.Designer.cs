﻿namespace fTimSach
{
    partial class fMuonSach
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMuon = new System.Windows.Forms.Button();
            this.btnViewHistory = new System.Windows.Forms.Button();
            this.btnFind = new System.Windows.Forms.Button();
            this.txbInfo = new System.Windows.Forms.TextBox();
            this.cbChedotim = new System.Windows.Forms.ComboBox();
            this.cbTheloai = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ckbTheloai = new System.Windows.Forms.CheckBox();
            this.pnDadangnhap = new System.Windows.Forms.Panel();
            this.btnBohet = new System.Windows.Forms.Button();
            this.dtpkNgaytra = new System.Windows.Forms.DateTimePicker();
            this.dtpkNgaymuon = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.txbDiachi = new System.Windows.Forms.TextBox();
            this.txbMakh = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txbHoten = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnRemove = new System.Windows.Forms.Button();
            this.listSach = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnAdd = new System.Windows.Forms.Button();
            this.dtgvSach = new System.Windows.Forms.DataGridView();
            this.btnAccept = new System.Windows.Forms.Button();
            this.txMaKH = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.txbMasach = new System.Windows.Forms.TextBox();
            this.txbTensach = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnQuanLi = new System.Windows.Forms.Panel();
            this.txbKiemTraID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.pnDadangnhap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvSach)).BeginInit();
            this.panel3.SuspendLayout();
            this.pnQuanLi.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnMuon
            // 
            this.btnMuon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnMuon.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnMuon.FlatAppearance.BorderSize = 3;
            this.btnMuon.Location = new System.Drawing.Point(249, 458);
            this.btnMuon.Name = "btnMuon";
            this.btnMuon.Size = new System.Drawing.Size(94, 36);
            this.btnMuon.TabIndex = 5;
            this.btnMuon.Text = "Đăng kí mượn";
            this.btnMuon.UseVisualStyleBackColor = false;
            this.btnMuon.Click += new System.EventHandler(this.btnMuon_Click_1);
            // 
            // btnViewHistory
            // 
            this.btnViewHistory.Location = new System.Drawing.Point(444, 46);
            this.btnViewHistory.Name = "btnViewHistory";
            this.btnViewHistory.Size = new System.Drawing.Size(120, 23);
            this.btnViewHistory.TabIndex = 5;
            this.btnViewHistory.Text = "Xem lịch sử mượn";
            this.btnViewHistory.UseVisualStyleBackColor = true;
            this.btnViewHistory.Click += new System.EventHandler(this.btnXemlichsumuon_Click);
            // 
            // btnFind
            // 
            this.btnFind.FlatAppearance.BorderColor = System.Drawing.Color.MediumSpringGreen;
            this.btnFind.FlatAppearance.BorderSize = 3;
            this.btnFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFind.Location = new System.Drawing.Point(3, 16);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(71, 26);
            this.btnFind.TabIndex = 4;
            this.btnFind.Text = "Tìm";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnTim_Click);
            // 
            // txbInfo
            // 
            this.txbInfo.Location = new System.Drawing.Point(89, 20);
            this.txbInfo.Name = "txbInfo";
            this.txbInfo.Size = new System.Drawing.Size(269, 20);
            this.txbInfo.TabIndex = 1;
            // 
            // cbChedotim
            // 
            this.cbChedotim.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChedotim.FormattingEnabled = true;
            this.cbChedotim.Items.AddRange(new object[] {
            "Mã sách",
            "Tên sách",
            "Tên tác giả"});
            this.cbChedotim.Location = new System.Drawing.Point(140, 49);
            this.cbChedotim.Name = "cbChedotim";
            this.cbChedotim.Size = new System.Drawing.Size(110, 21);
            this.cbChedotim.TabIndex = 2;
            // 
            // cbTheloai
            // 
            this.cbTheloai.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTheloai.FormattingEnabled = true;
            this.cbTheloai.Location = new System.Drawing.Point(348, 49);
            this.cbTheloai.Name = "cbTheloai";
            this.cbTheloai.Size = new System.Drawing.Size(180, 21);
            this.cbTheloai.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Tìm theo";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ckbTheloai);
            this.panel1.Controls.Add(this.btnFind);
            this.panel1.Controls.Add(this.txbInfo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cbChedotim);
            this.panel1.Controls.Add(this.cbTheloai);
            this.panel1.Location = new System.Drawing.Point(15, 76);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(549, 84);
            this.panel1.TabIndex = 8;
            // 
            // ckbTheloai
            // 
            this.ckbTheloai.AutoSize = true;
            this.ckbTheloai.Location = new System.Drawing.Point(278, 52);
            this.ckbTheloai.Name = "ckbTheloai";
            this.ckbTheloai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ckbTheloai.Size = new System.Drawing.Size(64, 17);
            this.ckbTheloai.TabIndex = 8;
            this.ckbTheloai.Text = "Thể loại";
            this.ckbTheloai.UseVisualStyleBackColor = true;
            // 
            // pnDadangnhap
            // 
            this.pnDadangnhap.Controls.Add(this.btnBohet);
            this.pnDadangnhap.Controls.Add(this.dtpkNgaytra);
            this.pnDadangnhap.Controls.Add(this.dtpkNgaymuon);
            this.pnDadangnhap.Controls.Add(this.label10);
            this.pnDadangnhap.Controls.Add(this.txbDiachi);
            this.pnDadangnhap.Controls.Add(this.txbMakh);
            this.pnDadangnhap.Controls.Add(this.label9);
            this.pnDadangnhap.Controls.Add(this.label7);
            this.pnDadangnhap.Controls.Add(this.label6);
            this.pnDadangnhap.Controls.Add(this.txbHoten);
            this.pnDadangnhap.Controls.Add(this.label4);
            this.pnDadangnhap.Controls.Add(this.label3);
            this.pnDadangnhap.Controls.Add(this.btnRemove);
            this.pnDadangnhap.Controls.Add(this.listSach);
            this.pnDadangnhap.Controls.Add(this.btnAdd);
            this.pnDadangnhap.Controls.Add(this.btnMuon);
            this.pnDadangnhap.Location = new System.Drawing.Point(567, 46);
            this.pnDadangnhap.Name = "pnDadangnhap";
            this.pnDadangnhap.Size = new System.Drawing.Size(432, 515);
            this.pnDadangnhap.TabIndex = 9;
            // 
            // btnBohet
            // 
            this.btnBohet.Location = new System.Drawing.Point(68, 261);
            this.btnBohet.Name = "btnBohet";
            this.btnBohet.Size = new System.Drawing.Size(53, 34);
            this.btnBohet.TabIndex = 22;
            this.btnBohet.Text = "Bỏ hết";
            this.btnBohet.UseVisualStyleBackColor = true;
            this.btnBohet.Click += new System.EventHandler(this.btnBohet_Click);
            // 
            // dtpkNgaytra
            // 
            this.dtpkNgaytra.CustomFormat = "dd/MM/yyyy";
            this.dtpkNgaytra.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpkNgaytra.Location = new System.Drawing.Point(321, 415);
            this.dtpkNgaytra.Name = "dtpkNgaytra";
            this.dtpkNgaytra.Size = new System.Drawing.Size(99, 20);
            this.dtpkNgaytra.TabIndex = 21;
            // 
            // dtpkNgaymuon
            // 
            this.dtpkNgaymuon.CustomFormat = "dd/MM/yyyy";
            this.dtpkNgaymuon.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpkNgaymuon.Location = new System.Drawing.Point(164, 416);
            this.dtpkNgaymuon.Name = "dtpkNgaymuon";
            this.dtpkNgaymuon.Size = new System.Drawing.Size(98, 20);
            this.dtpkNgaymuon.TabIndex = 20;
            this.dtpkNgaymuon.Value = new System.DateTime(2018, 11, 12, 0, 0, 0, 0);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(84, 100);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Địa chỉ:";
            // 
            // txbDiachi
            // 
            this.txbDiachi.Location = new System.Drawing.Point(133, 97);
            this.txbDiachi.Name = "txbDiachi";
            this.txbDiachi.Size = new System.Drawing.Size(270, 20);
            this.txbDiachi.TabIndex = 18;
            // 
            // txbMakh
            // 
            this.txbMakh.Location = new System.Drawing.Point(133, 71);
            this.txbMakh.Name = "txbMakh";
            this.txbMakh.ReadOnly = true;
            this.txbMakh.Size = new System.Drawing.Size(84, 20);
            this.txbMakh.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(84, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Mã KH:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(268, 422);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Ngày trả";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(97, 416);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Ngày mượn";
            // 
            // txbHoten
            // 
            this.txbHoten.Location = new System.Drawing.Point(133, 40);
            this.txbHoten.Name = "txbHoten";
            this.txbHoten.ReadOnly = true;
            this.txbHoten.Size = new System.Drawing.Size(221, 20);
            this.txbHoten.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(82, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Họ tên :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(137, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(148, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "PHIẾU MƯỢN SÁCH";
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(68, 212);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(53, 34);
            this.btnRemove.TabIndex = 1;
            this.btnRemove.Text = "Bỏ";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // listSach
            // 
            this.listSach.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listSach.FullRowSelect = true;
            this.listSach.GridLines = true;
            this.listSach.Location = new System.Drawing.Point(133, 127);
            this.listSach.MultiSelect = false;
            this.listSach.Name = "listSach";
            this.listSach.Size = new System.Drawing.Size(287, 257);
            this.listSach.TabIndex = 6;
            this.listSach.UseCompatibleStateImageBehavior = false;
            this.listSach.View = System.Windows.Forms.View.Details;
            this.listSach.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.listSach_ItemSelectionChanged);
            this.listSach.SelectedIndexChanged += new System.EventHandler(this.listSach_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Tên sách";
            this.columnHeader1.Width = 180;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Mã sách";
            this.columnHeader2.Width = 89;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(68, 161);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(53, 36);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // dtgvSach
            // 
            this.dtgvSach.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dtgvSach.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvSach.Location = new System.Drawing.Point(12, 201);
            this.dtgvSach.MultiSelect = false;
            this.dtgvSach.Name = "dtgvSach";
            this.dtgvSach.ReadOnly = true;
            this.dtgvSach.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgvSach.Size = new System.Drawing.Size(617, 350);
            this.dtgvSach.TabIndex = 14;
            this.dtgvSach.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgvSach_CellClick);
            // 
            // btnAccept
            // 
            this.btnAccept.Location = new System.Drawing.Point(189, 9);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(66, 23);
            this.btnAccept.TabIndex = 7;
            this.btnAccept.Text = "Xác nhận";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.button1_Click);
            // 
            // txMaKH
            // 
            this.txMaKH.Location = new System.Drawing.Point(77, 11);
            this.txMaKH.Name = "txMaKH";
            this.txMaKH.Size = new System.Drawing.Size(105, 20);
            this.txMaKH.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.txbMasach);
            this.panel3.Controls.Add(this.txbTensach);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Location = new System.Drawing.Point(15, 167);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(613, 24);
            this.panel3.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(474, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Mã sách";
            // 
            // txbMasach
            // 
            this.txbMasach.Location = new System.Drawing.Point(528, 3);
            this.txbMasach.Name = "txbMasach";
            this.txbMasach.ReadOnly = true;
            this.txbMasach.Size = new System.Drawing.Size(82, 20);
            this.txbMasach.TabIndex = 2;
            // 
            // txbTensach
            // 
            this.txbTensach.Location = new System.Drawing.Point(69, 1);
            this.txbTensach.Name = "txbTensach";
            this.txbTensach.ReadOnly = true;
            this.txbTensach.Size = new System.Drawing.Size(399, 20);
            this.txbTensach.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Đang chọn";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Nhập mã KH";
            // 
            // pnQuanLi
            // 
            this.pnQuanLi.Controls.Add(this.label2);
            this.pnQuanLi.Controls.Add(this.txMaKH);
            this.pnQuanLi.Controls.Add(this.btnAccept);
            this.pnQuanLi.Location = new System.Drawing.Point(707, -2);
            this.pnQuanLi.Name = "pnQuanLi";
            this.pnQuanLi.Size = new System.Drawing.Size(269, 42);
            this.pnQuanLi.TabIndex = 15;
            // 
            // txbKiemTraID
            // 
            this.txbKiemTraID.Location = new System.Drawing.Point(15, 34);
            this.txbKiemTraID.Name = "txbKiemTraID";
            this.txbKiemTraID.ReadOnly = true;
            this.txbKiemTraID.Size = new System.Drawing.Size(100, 20);
            this.txbKiemTraID.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Mã người dùng";
            // 
            // fMuonSach
            // 
            this.AcceptButton = this.btnFind;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(999, 563);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txbKiemTraID);
            this.Controls.Add(this.pnQuanLi);
            this.Controls.Add(this.btnViewHistory);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.dtgvSach);
            this.Controls.Add(this.pnDadangnhap);
            this.Controls.Add(this.panel1);
            this.Name = "fMuonSach";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tìm sách";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.fTimSach_FormClosing);
            this.Load += new System.EventHandler(this.fTimSach_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnDadangnhap.ResumeLayout(false);
            this.pnDadangnhap.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvSach)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.pnQuanLi.ResumeLayout(false);
            this.pnQuanLi.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMuon;
        private System.Windows.Forms.Button btnViewHistory;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.TextBox txbInfo;
        private System.Windows.Forms.ComboBox cbTheloai;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbChedotim;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnDadangnhap;
        private System.Windows.Forms.ListView listSach;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.DataGridView dtgvSach;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txbDiachi;
        private System.Windows.Forms.TextBox txbMakh;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txbHoten;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txbMasach;
        private System.Windows.Forms.TextBox txbTensach;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtpkNgaytra;
        private System.Windows.Forms.DateTimePicker dtpkNgaymuon;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox ckbTheloai;
        private System.Windows.Forms.TextBox txMaKH;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnBohet;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnQuanLi;
        private System.Windows.Forms.TextBox txbKiemTraID;
        private System.Windows.Forms.Label label5;
    }
}

