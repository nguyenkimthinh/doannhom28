﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using fTimSach.DAO;

namespace fTimSach
{
       
    public partial class fTimSach : Form
    {
        
        public fTimSach()
        {
            InitializeComponent();
            loadDatetime();
            loadTheloai();
            setDefault();
        }
       
        void setDefault()
        {
            cbChedotim.SelectedIndex = 0;
        }
       
        void loadTheloai()
        {
            string query = "SELECT DISTINCT TheLoai FROM SACH";
            
           cbTheloai.DataSource = DataProvider.Instance.ExecuteQuery(query);
           
           cbTheloai.DisplayMember = "TheLoai";
          
        }
       
        void loadDatetime() // set systemtime for datetimepicker
        {
            dtpkNgaymuon.Value = DateTime.Now;
            dtpkNgaytra.Value = DateTime.Now;    
        }
        private void fTimSach_Load(object sender, EventArgs e)
        {

        }

        private void btnXemlichsumuon_Click(object sender, EventArgs e)
        {
            fXemHistory fxemhistory = new fXemHistory();
            fxemhistory.ShowDialog();
        }

        private void btnMuon_Click_1(object sender, EventArgs e)
        {
           if (MessageBox.Show("Xác nhận phiếu mượn", "Xác nhận", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
           {
               MessageBox.Show("Đăng kí phiếu mượn thành công","Thông báo");
               listSach.ResetText();
           }
        }
        private void fTimSach_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn thoát chương trình?", "Thông báo", MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK)
                e.Cancel = true;
        }

      

        private void btnTim_Click(object sender, EventArgs e)
        {
            TraCuu(cbChedotim.Text, txbTimsach.Text, cbTheloai.Text);
           
        }

        void TraCuu(string loaiTk, string text, string theLoai)
        {
            string query ="";
            if (loaiTk == "Tên sách")
                loaiTk = "TenSach";
            else
                loaiTk = "TacGia";
            if (ckbTheloai.Checked)
                query = string.Format("SELECT TenSach as [Tên sách], TacGia as [Tác giả], TheLoai as [Thể loại], GiaSach as [Giá], NhaXuatBan as [Nhà xuất bản], TinhTrang as [Tình trạng], MaSach as [Mã sách] FROM SACH WHERE {0} like N'%{1}%'AND TheLoai = N'{2}'", loaiTk, text, theLoai);
            else
                query = string.Format("SELECT TenSach as [Tên sách], TacGia as [Tác giả], TheLoai as [Thể loại], GiaSach as [Giá], NhaXuatBan as [Nhà xuất bản], TinhTrang as [Tình trạng], MaSach as [Mã sách] FROM SACH WHERE {0} like N'%{1}%'", loaiTk, text);
            dtgvSach.DataSource = DataProvider.Instance.ExecuteQuery(query);
        }
        
    }
}
