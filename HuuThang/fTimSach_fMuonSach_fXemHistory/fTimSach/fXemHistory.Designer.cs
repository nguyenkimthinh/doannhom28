﻿namespace fTimSach
{
    partial class fXemHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.lb1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.tbLichsumuon = new System.Windows.Forms.TabControl();
            this.tpDangmuon = new System.Windows.Forms.TabPage();
            this.dtgvDangmuon = new System.Windows.Forms.DataGridView();
            this.tpSachdatra = new System.Windows.Forms.TabPage();
            this.dtgvDatra = new System.Windows.Forms.DataGridView();
            this.txbMadocgia = new System.Windows.Forms.TextBox();
            this.btnXem = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.tbLichsumuon.SuspendLayout();
            this.tpDangmuon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvDangmuon)).BeginInit();
            this.tpSachdatra.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvDatra)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txbMadocgia);
            this.panel2.Controls.Add(this.lb1);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Location = new System.Drawing.Point(393, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(244, 37);
            this.panel2.TabIndex = 12;
            // 
            // lb1
            // 
            this.lb1.AutoSize = true;
            this.lb1.Location = new System.Drawing.Point(100, 11);
            this.lb1.Name = "lb1";
            this.lb1.Size = new System.Drawing.Size(61, 13);
            this.lb1.TabIndex = 1;
            this.lb1.Text = "Mã độc giả";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Chào, ";
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(13, 17);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 14;
            this.btnBack.Text = "Trở về";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // tbLichsumuon
            // 
            this.tbLichsumuon.Controls.Add(this.tpDangmuon);
            this.tbLichsumuon.Controls.Add(this.tpSachdatra);
            this.tbLichsumuon.Location = new System.Drawing.Point(13, 72);
            this.tbLichsumuon.Name = "tbLichsumuon";
            this.tbLichsumuon.SelectedIndex = 0;
            this.tbLichsumuon.Size = new System.Drawing.Size(624, 332);
            this.tbLichsumuon.TabIndex = 15;
            // 
            // tpDangmuon
            // 
            this.tpDangmuon.Controls.Add(this.dtgvDangmuon);
            this.tpDangmuon.Location = new System.Drawing.Point(4, 22);
            this.tpDangmuon.Name = "tpDangmuon";
            this.tpDangmuon.Padding = new System.Windows.Forms.Padding(3);
            this.tpDangmuon.Size = new System.Drawing.Size(616, 306);
            this.tpDangmuon.TabIndex = 0;
            this.tpDangmuon.Text = "Sách đang mượn";
            this.tpDangmuon.UseVisualStyleBackColor = true;
            // 
            // dtgvDangmuon
            // 
            this.dtgvDangmuon.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvDangmuon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvDangmuon.Location = new System.Drawing.Point(7, 35);
            this.dtgvDangmuon.Name = "dtgvDangmuon";
            this.dtgvDangmuon.ReadOnly = true;
            this.dtgvDangmuon.Size = new System.Drawing.Size(603, 265);
            this.dtgvDangmuon.TabIndex = 0;
            // 
            // tpSachdatra
            // 
            this.tpSachdatra.Controls.Add(this.dtgvDatra);
            this.tpSachdatra.Location = new System.Drawing.Point(4, 22);
            this.tpSachdatra.Name = "tpSachdatra";
            this.tpSachdatra.Padding = new System.Windows.Forms.Padding(3);
            this.tpSachdatra.Size = new System.Drawing.Size(616, 306);
            this.tpSachdatra.TabIndex = 1;
            this.tpSachdatra.Text = "Sách đã trả";
            this.tpSachdatra.UseVisualStyleBackColor = true;
            // 
            // dtgvDatra
            // 
            this.dtgvDatra.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvDatra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvDatra.Location = new System.Drawing.Point(7, 35);
            this.dtgvDatra.Name = "dtgvDatra";
            this.dtgvDatra.ReadOnly = true;
            this.dtgvDatra.Size = new System.Drawing.Size(603, 265);
            this.dtgvDatra.TabIndex = 1;
            // 
            // txbMadocgia
            // 
            this.txbMadocgia.Location = new System.Drawing.Point(167, 8);
            this.txbMadocgia.Name = "txbMadocgia";
            this.txbMadocgia.Size = new System.Drawing.Size(71, 20);
            this.txbMadocgia.TabIndex = 2;
            // 
            // btnXem
            // 
            this.btnXem.Location = new System.Drawing.Point(562, 55);
            this.btnXem.Name = "btnXem";
            this.btnXem.Size = new System.Drawing.Size(75, 23);
            this.btnXem.TabIndex = 16;
            this.btnXem.Text = "Xem";
            this.btnXem.UseVisualStyleBackColor = true;
            this.btnXem.Click += new System.EventHandler(this.btnXem_Click);
            // 
            // fXemHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(657, 409);
            this.Controls.Add(this.btnXem);
            this.Controls.Add(this.tbLichsumuon);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.panel2);
            this.Name = "fXemHistory";
            this.Text = "Lịch sử mượn";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tbLichsumuon.ResumeLayout(false);
            this.tpDangmuon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvDangmuon)).EndInit();
            this.tpSachdatra.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvDatra)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lb1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.TabControl tbLichsumuon;
        private System.Windows.Forms.TabPage tpDangmuon;
        private System.Windows.Forms.DataGridView dtgvDangmuon;
        private System.Windows.Forms.TabPage tpSachdatra;
        private System.Windows.Forms.DataGridView dtgvDatra;
        private System.Windows.Forms.TextBox txbMadocgia;
        private System.Windows.Forms.Button btnXem;
    }
}