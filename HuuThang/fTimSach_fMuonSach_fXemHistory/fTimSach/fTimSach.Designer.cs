﻿namespace fTimSach
{
    partial class fTimSach
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMuon = new System.Windows.Forms.Button();
            this.btnXemlichsumuon = new System.Windows.Forms.Button();
            this.btnTim = new System.Windows.Forms.Button();
            this.txbTimsach = new System.Windows.Forms.TextBox();
            this.cbChedotim = new System.Windows.Forms.ComboBox();
            this.cbTheloai = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ckbTheloai = new System.Windows.Forms.CheckBox();
            this.pnDadangnhap = new System.Windows.Forms.Panel();
            this.dtpkNgaytra = new System.Windows.Forms.DateTimePicker();
            this.dtpkNgaymuon = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.txbMakh = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lbMaphieu = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnRemove = new System.Windows.Forms.Button();
            this.listSach = new System.Windows.Forms.ListView();
            this.btnAdd = new System.Windows.Forms.Button();
            this.dtgvSach = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txbUsername = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txbTinhtrang = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txbMasach = new System.Windows.Forms.TextBox();
            this.txbTensach = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.pnDadangnhap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvSach)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnMuon
            // 
            this.btnMuon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnMuon.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnMuon.FlatAppearance.BorderSize = 3;
            this.btnMuon.Location = new System.Drawing.Point(191, 464);
            this.btnMuon.Name = "btnMuon";
            this.btnMuon.Size = new System.Drawing.Size(94, 36);
            this.btnMuon.TabIndex = 5;
            this.btnMuon.Text = "Đăng kí mượn";
            this.btnMuon.UseVisualStyleBackColor = false;
            this.btnMuon.Click += new System.EventHandler(this.btnMuon_Click_1);
            // 
            // btnXemlichsumuon
            // 
            this.btnXemlichsumuon.Location = new System.Drawing.Point(186, 8);
            this.btnXemlichsumuon.Name = "btnXemlichsumuon";
            this.btnXemlichsumuon.Size = new System.Drawing.Size(120, 23);
            this.btnXemlichsumuon.TabIndex = 5;
            this.btnXemlichsumuon.Text = "Xem lịch sử mượn";
            this.btnXemlichsumuon.UseVisualStyleBackColor = true;
            this.btnXemlichsumuon.Click += new System.EventHandler(this.btnXemlichsumuon_Click);
            // 
            // btnTim
            // 
            this.btnTim.FlatAppearance.BorderColor = System.Drawing.Color.MediumSpringGreen;
            this.btnTim.FlatAppearance.BorderSize = 3;
            this.btnTim.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTim.Location = new System.Drawing.Point(3, 16);
            this.btnTim.Name = "btnTim";
            this.btnTim.Size = new System.Drawing.Size(71, 26);
            this.btnTim.TabIndex = 4;
            this.btnTim.Text = "Tìm";
            this.btnTim.UseVisualStyleBackColor = true;
            this.btnTim.Click += new System.EventHandler(this.btnTim_Click);
            // 
            // txbTimsach
            // 
            this.txbTimsach.Location = new System.Drawing.Point(89, 20);
            this.txbTimsach.Name = "txbTimsach";
            this.txbTimsach.Size = new System.Drawing.Size(269, 20);
            this.txbTimsach.TabIndex = 1;
            // 
            // cbChedotim
            // 
            this.cbChedotim.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChedotim.FormattingEnabled = true;
            this.cbChedotim.Items.AddRange(new object[] {
            "Tên sách",
            "Tên tác giả"});
            this.cbChedotim.Location = new System.Drawing.Point(140, 49);
            this.cbChedotim.Name = "cbChedotim";
            this.cbChedotim.Size = new System.Drawing.Size(110, 21);
            this.cbChedotim.TabIndex = 2;
            // 
            // cbTheloai
            // 
            this.cbTheloai.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTheloai.FormattingEnabled = true;
            this.cbTheloai.Location = new System.Drawing.Point(348, 49);
            this.cbTheloai.Name = "cbTheloai";
            this.cbTheloai.Size = new System.Drawing.Size(180, 21);
            this.cbTheloai.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Tìm theo";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ckbTheloai);
            this.panel1.Controls.Add(this.btnTim);
            this.panel1.Controls.Add(this.txbTimsach);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cbChedotim);
            this.panel1.Controls.Add(this.cbTheloai);
            this.panel1.Location = new System.Drawing.Point(12, 51);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(549, 84);
            this.panel1.TabIndex = 8;
            // 
            // ckbTheloai
            // 
            this.ckbTheloai.AutoSize = true;
            this.ckbTheloai.Location = new System.Drawing.Point(278, 52);
            this.ckbTheloai.Name = "ckbTheloai";
            this.ckbTheloai.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ckbTheloai.Size = new System.Drawing.Size(64, 17);
            this.ckbTheloai.TabIndex = 8;
            this.ckbTheloai.Text = "Thể loại";
            this.ckbTheloai.UseVisualStyleBackColor = true;
            // 
            // pnDadangnhap
            // 
            this.pnDadangnhap.Controls.Add(this.dtpkNgaytra);
            this.pnDadangnhap.Controls.Add(this.dtpkNgaymuon);
            this.pnDadangnhap.Controls.Add(this.label10);
            this.pnDadangnhap.Controls.Add(this.textBox2);
            this.pnDadangnhap.Controls.Add(this.txbMakh);
            this.pnDadangnhap.Controls.Add(this.label9);
            this.pnDadangnhap.Controls.Add(this.lbMaphieu);
            this.pnDadangnhap.Controls.Add(this.label8);
            this.pnDadangnhap.Controls.Add(this.label7);
            this.pnDadangnhap.Controls.Add(this.label6);
            this.pnDadangnhap.Controls.Add(this.textBox1);
            this.pnDadangnhap.Controls.Add(this.label4);
            this.pnDadangnhap.Controls.Add(this.label3);
            this.pnDadangnhap.Controls.Add(this.btnRemove);
            this.pnDadangnhap.Controls.Add(this.listSach);
            this.pnDadangnhap.Controls.Add(this.btnAdd);
            this.pnDadangnhap.Controls.Add(this.btnMuon);
            this.pnDadangnhap.Location = new System.Drawing.Point(567, 8);
            this.pnDadangnhap.Name = "pnDadangnhap";
            this.pnDadangnhap.Size = new System.Drawing.Size(409, 515);
            this.pnDadangnhap.TabIndex = 9;
            
            // 
            // dtpkNgaytra
            // 
            this.dtpkNgaytra.CustomFormat = "dd/MM/yyyy";
            this.dtpkNgaytra.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpkNgaytra.Location = new System.Drawing.Point(291, 440);
            this.dtpkNgaytra.Name = "dtpkNgaytra";
            this.dtpkNgaytra.Size = new System.Drawing.Size(99, 20);
            this.dtpkNgaytra.TabIndex = 21;
            // 
            // dtpkNgaymuon
            // 
            this.dtpkNgaymuon.CustomFormat = "dd/MM/yyyy";
            this.dtpkNgaymuon.Enabled = false;
            this.dtpkNgaymuon.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpkNgaymuon.Location = new System.Drawing.Point(140, 440);
            this.dtpkNgaymuon.Name = "dtpkNgaymuon";
            this.dtpkNgaymuon.Size = new System.Drawing.Size(98, 20);
            this.dtpkNgaymuon.TabIndex = 20;
            this.dtpkNgaymuon.Value = new System.DateTime(2018, 11, 12, 0, 0, 0, 0);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(84, 100);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Địa chỉ:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(133, 97);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(256, 20);
            this.textBox2.TabIndex = 18;
            // 
            // txbMakh
            // 
            this.txbMakh.Location = new System.Drawing.Point(133, 71);
            this.txbMakh.Name = "txbMakh";
            this.txbMakh.ReadOnly = true;
            this.txbMakh.Size = new System.Drawing.Size(112, 20);
            this.txbMakh.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(84, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Mã KH:";
            // 
            // lbMaphieu
            // 
            this.lbMaphieu.AutoSize = true;
            this.lbMaphieu.Location = new System.Drawing.Point(65, 17);
            this.lbMaphieu.Name = "lbMaphieu";
            this.lbMaphieu.Size = new System.Drawing.Size(46, 13);
            this.lbMaphieu.TabIndex = 15;
            this.lbMaphieu.Text = ". . . . . . .";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Mã phiếu:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(308, 424);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Ngày trả";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(156, 424);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Ngày mượn";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(133, 40);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(186, 20);
            this.textBox1.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(82, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Họ tên :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(137, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(148, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "PHIẾU MƯỢN SÁCH";
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(68, 212);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(53, 34);
            this.btnRemove.TabIndex = 1;
            this.btnRemove.Text = "Bỏ";
            this.btnRemove.UseVisualStyleBackColor = true;
            // 
            // listSach
            // 
            this.listSach.Location = new System.Drawing.Point(133, 138);
            this.listSach.Name = "listSach";
            this.listSach.Size = new System.Drawing.Size(256, 283);
            this.listSach.TabIndex = 6;
            this.listSach.UseCompatibleStateImageBehavior = false;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(68, 161);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(53, 36);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // dtgvSach
            // 
            this.dtgvSach.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dtgvSach.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvSach.Location = new System.Drawing.Point(12, 169);
            this.dtgvSach.MultiSelect = false;
            this.dtgvSach.Name = "dtgvSach";
            this.dtgvSach.ReadOnly = true;
            this.dtgvSach.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgvSach.Size = new System.Drawing.Size(617, 350);
            this.dtgvSach.TabIndex = 10;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txbUsername);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.btnXemlichsumuon);
            this.panel2.Location = new System.Drawing.Point(248, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(313, 37);
            this.panel2.TabIndex = 11;
            // 
            // txbUsername
            // 
            this.txbUsername.Location = new System.Drawing.Point(106, 10);
            this.txbUsername.Name = "txbUsername";
            this.txbUsername.Size = new System.Drawing.Size(74, 20);
            this.txbUsername.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Chào, ";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txbTinhtrang);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.txbMasach);
            this.panel3.Controls.Add(this.txbTensach);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Location = new System.Drawing.Point(12, 139);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(613, 24);
            this.panel3.TabIndex = 12;
            // 
            // txbTinhtrang
            // 
            this.txbTinhtrang.Location = new System.Drawing.Point(555, 3);
            this.txbTinhtrang.Name = "txbTinhtrang";
            this.txbTinhtrang.ReadOnly = true;
            this.txbTinhtrang.Size = new System.Drawing.Size(55, 20);
            this.txbTinhtrang.TabIndex = 6;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(494, 5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 5;
            this.label13.Text = "Tình trạng";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(368, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Mã sách";
            // 
            // txbMasach
            // 
            this.txbMasach.Location = new System.Drawing.Point(419, 3);
            this.txbMasach.Name = "txbMasach";
            this.txbMasach.ReadOnly = true;
            this.txbMasach.Size = new System.Drawing.Size(69, 20);
            this.txbMasach.TabIndex = 2;
            // 
            // txbTensach
            // 
            this.txbTensach.Location = new System.Drawing.Point(69, 1);
            this.txbTensach.Name = "txbTensach";
            this.txbTensach.ReadOnly = true;
            this.txbTensach.Size = new System.Drawing.Size(289, 20);
            this.txbTensach.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Đang chọn";
            // 
            // fTimSach
            // 
            this.AcceptButton = this.btnTim;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(978, 531);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.dtgvSach);
            this.Controls.Add(this.pnDadangnhap);
            this.Controls.Add(this.panel1);
            this.Name = "fTimSach";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tìm sách";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.fTimSach_FormClosing);
            this.Load += new System.EventHandler(this.fTimSach_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnDadangnhap.ResumeLayout(false);
            this.pnDadangnhap.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvSach)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnMuon;
        private System.Windows.Forms.Button btnXemlichsumuon;
        private System.Windows.Forms.Button btnTim;
        private System.Windows.Forms.TextBox txbTimsach;
        private System.Windows.Forms.ComboBox cbTheloai;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbChedotim;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnDadangnhap;
        private System.Windows.Forms.ListView listSach;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.DataGridView dtgvSach;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox txbMakh;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbMaphieu;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txbMasach;
        private System.Windows.Forms.TextBox txbTensach;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtpkNgaytra;
        private System.Windows.Forms.DateTimePicker dtpkNgaymuon;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txbTinhtrang;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox ckbTheloai;
        private System.Windows.Forms.TextBox txbUsername;
    }
}

