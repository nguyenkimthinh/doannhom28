﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using fTimSach.DAO;

namespace fTimSach
{
    public partial class fXemHistory : Form
    {
        public fXemHistory()
        {
            InitializeComponent();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        void LoadSachdangmuon(string madg)
        {
            string query = string.Format("SELECT TenSach, MaPhieu, NgayMuon, NgayPhaiTra FROM SACH, (SELECT * FROM PHIEUMUON pm WHERE pm.MaPhieu NOT IN (SELECT MaPhieu FROM PHIEUTRA) AND MaDocGia = {0}) AS TEMPT WHERE SACH.MaSach = TEMPT.MaSach", madg);
            dtgvDangmuon.DataSource = DataProvider.Instance.ExecuteQuery(query);
        }
        void LoadDatra(string madg)
        {
            string query = string.Format("SELECT TenSach, MaPhieu, NgayMuon, NgayTra FROM SACH, (SELECT PHIEUTRA.MaPhieu, PHIEUTRA.MaDocGia, PHIEUTRA.MaSach, NgayMuon, NgayTra FROM PHIEUTRA, PHIEUMUON  WHERE PHIEUTRA.MaPhieu = PHIEUMUON.MaPhieu and PHIEUTRA.MaDocGia = {0}) AS TEMPT WHERE SACH.MaSach = TEMPT.MaSach", madg);
            dtgvDatra.DataSource = DataProvider.Instance.ExecuteQuery(query);
        }

        private void btnXem_Click(object sender, EventArgs e)
        {
            LoadSachdangmuon(txbMadocgia.Text);
            LoadDatra(txbMadocgia.Text);
            
        }


       
    }
}
