USE MASTER
GO
IF DB_ID ('QLThuVien') IS NOT NULL
	DROP DATABASE QLThuVien
GO

CREATE DATABASE QLThuVien
GO 

USE QLThuVien
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF DB_ID ('UF_AUTO_maKH') IS NOT NULL
	DROP function UF_AUTO_maKH
GO

CREATE FUNCTION UF_AUTO_maKH()
RETURNS int
AS
BEGIN
	DECLARE @maKH int
	IF EXISTS (SELECT maKH
				FROM dbo.TaiKhoan
				WHERE maKH= 1)
	BEGIN
		SELECT @maKH = MAX(MaKH) + 1
		FROM dbo.TaiKhoan
	END
	ELSE
		SET @maKH=1
	RETURN @maKH
END 

--INSERT TaiKhoan
--values (dbo.UF_AUTO_maKH(), N'Như Thuỷ', N'Nữ', NULL, NULL,'ThuyTran', 'Thuy1234', 0)

create TABLE TaiKhoan
(
	MaKH int
	DEFAULT dbo.UF_AUTO_maKH(),
	Hoten NVARCHAR(30),
	GioiTinh nchar(5),
	Email varchar(30),
	SDT NCHAR(10),
	UserName VARCHAR(30),
	MyPassWord VARCHAR (20),
	PhanQuyen INT
	CONSTRAINT PK_KhachHang
	PRIMARY KEY (MaKH),
	CONSTRAINT CK_GioiTinh
	CHECK (GioiTinh IN ('Nam', N'Nữ')),
	CONSTRAINT CK_PhanQuyen
	CHECK (PhanQuyen IN (0, 1))	--0: Admin, 1: KhachHang
)


ALTER Table TaiKhoan 
ADD CONSTRAINT UserName_DuyNhat UNIQUE (UserName)

CREATE TABLE Sach
(
	MaSach int,
	TheLoai NVARCHAR (30),
	TenSach NVARCHAR(50),
	TacGia NVARCHAR(50),
	GiaSach INT,
	SoLuongTon INT, 
	DoHot int,
	TinhTrang NVARCHAR(5)
	CONSTRAINT PK_Sach
	PRIMARY KEY (MaSach),
	CONSTRAINT CK_TinhTrang
	CHECK (TinhTrang IN (N'Cũ', N'Mới'))
)

CREATE TABLE PhieuMuon
(
	MaPhieu int,
	MaKH int,
	MaSach int,
	NgayMuon DATETIME,
	NgayPhaiTra DATETIME
)

CREATE TABLE PhieuTra
(
	MaPhieu int,
	MaKH int,
	MaSach int,
	NgayTra DATETIME,
	TongTien int
)

--KHOÁ NGOẠI

--Bảng PhieuMuon
ALTER TABLE dbo.PhieuMuon
ADD CONSTRAINT FK_PhieuMuon_KhachHang
FOREIGN KEY(MaKH)
REFERENCES dbo.TaiKhoan(maKH)

ALTER TABLE dbo.PhieuMuon
ADD CONSTRAINT FK_PhieuMuon_Sach
FOREIGN KEY(MaSach)
REFERENCES dbo.Sach(MaSach)

--Bảng PhieuTra
ALTER TABLE dbo.PhieuTra
ADD CONSTRAINT FK_PhieuTra_KhachHang
FOREIGN KEY(MaKH)
REFERENCES dbo.TaiKhoan(maKH)

ALTER TABLE dbo.PhieuTra
ADD CONSTRAINT FK_PhieuTra_Sach
FOREIGN KEY(MaSach)
REFERENCES dbo.Sach(MaSach)

--Insert dữ liệu.
--Khách Hàng
INSERT into TaiKhoan(MaKH, Hoten, Email, GioiTinh,SDT, UserName, MyPassword, PhanQuyen)
VALUES (1, N'Nguyễn Văn Hoàng', 'Hoang123@gmail.com', N'Nam','0932456711','NguyenVanHoang123', N'hoang123', 0)
INSERT into TaiKhoan(MaKH, Hoten, Email, GioiTinh,SDT, UserName, MyPassword, PhanQuyen)
VALUES (2, N'Lê Thị Hồng Mai', 'HongMai581@gmail.com', N'Nữ','0382459711','HongMai0987', N'maihoang09', 1)
INSERT into TaiKhoan(MaKH, Hoten, Email, GioiTinh,SDT, UserName, MyPassword, PhanQuyen)
VALUES (3, N'Trần Lê Vũ Tuấn', 'TranTuan3794@gmail.com', N'Nam','0348715971','TranTuan123', N'12345abc', 1)
INSERT into TaiKhoan(MaKH, Hoten, Email, GioiTinh,SDT, UserName, MyPassword, PhanQuyen)
VALUES (4, N'Nguyễn Thị Lan', 'LanNguyen2012@gmail.com', N'Nữ','0983476971','LanNguyen0912', N'haudll207', 0)
INSERT into TaiKhoan(MaKH, Hoten, Email, GioiTinh,SDT, UserName, MyPassword, PhanQuyen)
VALUES (5, N'Hoàng Xuân Nam','XuanNam0918@gmail.com' , N'Nam','092305971','XuanNam236', N'2466adhg', 0)
INSERT into TaiKhoan(MaKH, Hoten, Email, GioiTinh,SDT, UserName, MyPassword, PhanQuyen)
VALUES (6, N'Trần Linh Đan','LinhDan0918@gmail.com' , N'Nữ','092323481', 'LinhChau', N'Linh1234', 1)

--Sách
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (10001, N'Truyện ngắn', N'Cánh đồng bất tận', N'Nguyễn Ngọc Tư', 120000, 15, 3, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (10002, N'Truyện ngắn', N'Chí Phèo', N'Nam Cao', 13000, 12, 2, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (10003, N'Truyện ngắn', N'Vợ nhặt', N'Kim Lân', 80000, 8, 3, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (10004, N'Truyện ngắn', N'Tuổi thơ dữ dội', N'Phùng Quán', 25000, 15, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (10005, N'Truyện ngắn', N'Lão Hạc', N'Nam Cao', 140000, 5, 2, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (10006, N'Truyện ngắn', N'Đôi mắt', N'Nam Cao', 15500, 10, 4, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (10007, N'Truyện ngắn', N'Đời thừa', N'Nam Cao', 15500, 10, 4, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (10008, N'Truyện ngắn', N'Đám cưới', N'Nam Cao', 98000, 5, 2, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (10009, N'Truyện ngắn', N'Bảy bước tới mùa hè', N'Nguyễn Nhật Ánh', 35000, 5, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (10010, N'Truyện ngắn', N'Chút một ngày tốt lành', N'Nguyễn Nhật Ánh',49000, 10, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (10011, N'Truyện ngắn', N'Cô gái đến từ hôm qua', N'Nguyễn Nhật Ánh', 58000, 10, 2, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (10012, N'Truyện ngắn', N'Bồ câu không đưa thư', N'Nguyễn Nhật Ánh', 32000, 6, 2, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (10013, N'Truyện ngắn', N'Đảo mộng mơ', N'Nguyễn Nhật Ánh', 42000, 12, 3, N'Mới') 
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (10014, N'Truyện ngắn', N'Bong bóng lên trời', N'Nguyễn Nhật Ánh', 31000, 10, 3, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (10015, N'Truyện ngắn', N'Thương nhớ Trà Long', N'Nguyễn Nhật Ánh', 52000, 6, 2, N'Mới')

INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (20001, N'Thơ', N'Cây Bàng', N'Xuân Quỳnh', 12000, 10, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (20002, N'Thơ', N'Cô giáo của em', N'Xuân Quỳnh', 15000,3, 2, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (20003, N'Thơ', N'Tập tầm vong', N'Xuân Quỳnh', 22000, 5, 2, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (20004, N'Thơ', N'Sóng', N'Xuân Quỳnh', 18000, 12, 3, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (20006, N'Thơ', N'Giục giã', N'Xuân Diệu', 15000, 15, 2, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (20008, N'Thơ', N'Khúc mùa thu', N'Hùng Thanh Quang', 12000, 6, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (20009, N'Thơ', N'Em gầy như liễu yếu trong thơ cỗ', N'Nguyễn Sa', 12500, 6, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (20010, N'Thơ', N'Gái quê', N'Hàn Mạc Tử', 16000, 8, 2, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (20011, N'Thơ', N'Vội vàng', N'Xuân Diệu', 22500, 9, 2, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (20012, N'Thơ', N'Một tình yêu', N'Xuân Diệu', 42000, 10, 3, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (20013, N'Thơ', N'Đây thôn Vĩ Dạ', N'Hàn Mạc Tử', 25000, 11, 3, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (20014, N'Thơ', N'Ghen', N'Hàn Mạc Tử', 32000, 12, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (20015, N'Thơ', N'Bánh trôi nước', N'Hồ Xuân H', 17000, 13, 2, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (20007, N'Thơ', N'Cái quạt giấy', N'Hồ Xuân Hương', 26000, 7, 3, N'Cũ')

INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (30001, N'Truyện thiếu nhi', N'The River', N'Marc Martin', 35000, 7, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (30002, N'Truyện thiếu nhi', N'ĐôRêMon', N'Fujiko Fujio', 56000, 15, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (30003, N'Truyện thiếu nhi', N'Dế mèn phiêu lưu kí', N'Tô Hoài', 20000, 10, 3, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (30004, N'Truyện thiếu nhi', N'Đất rừng phương Nam', N'Đoàn Giỏi', 26000, 12, 2, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (30005, N'Truyện thiếu nhi', N'Gốc sân và khoảng trời', N'Trần Đăng Khoa', 31000, 20, 2, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (30006, N'Truyện thiếu nhi', N'Vì sao tớ yêu ông', N'Howarth', 56000, 17, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (30007, N'Truyện thiếu nhi', N'Cậu bé bút chì', N'Yoshito Usui', 56000, 9, 3, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (30008, N'Truyện thiếu nhi', N'Thám tử lừng danh CôNan', N'Aoyama Gosho', 36000, 21, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (30009, N'Truyện thiếu nhi', N'Thần đồng đất Việt', N'Lê Linh, công ty Phan Thị', 33000, 3, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (30010, N'Truyện thiếu nhi', N'Ai ở sau lưng bạn thế?', N'Accototo', 22000, 5, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (30011, N'Truyện thiếu nhi', N'Nhóc Miko', N'ONO Kerio', 16000, 12, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (30012, N'Truyện thiếu nhi', N'Tớ là mèo Pusheen', N'Claire Brlton', 52000, 8, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (30013, N'Truyện thiếu nhi', N'Con yêu bố chừng nào', N'Sam McBratney', 27000, 7, 3, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (30014, N'Truyện thiếu nhi', N'Tấm Cám', N'Truyện cổ tích', 15000, 7, 2, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (30015, N'Truyện thiếu nhi', N'Cô bé quàng khăn đỏ', N'Truyện nước ngoài', 18000, 7, 2, N'Cũ')

INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (40001, N'Sách giáo khoa', N'Tiếng Việt 1', N'Nhà xuất bản Giáo dục VN', 35000, 7, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (40002, N'Sách giáo khoa', N'Tiếng Việt 2', N'Nhà xuất bản Giáo dục VN', 56000, 15, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (40003, N'Sách giáo khoa', N'Tiếng Việt 3', N'Nhà xuất bản Giáo dục VN', 20000, 10, 3, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (40004, N'Sách giáo khoa', N'Tiếng Việt 4', N'Nhà xuất bản Giáo dục VN', 26000, 12, 2, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (40005, N'Sách giáo khoa', N'Tiếng Việt 5', N'Nhà xuất bản Giáo dục VN', 31000, 20, 2, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (40006, N'Sách giáo khoa', N'Toán 1', N'Nhà xuất bản Giáo dục VN', 56000, 17, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (40007, N'Sách giáo khoa', N'Toán 2', N'Nhà xuất bản Giáo dục VN', 56000, 9, 3, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (40008, N'Sách giáo khoa', N'Toán 3', N'Nhà xuất bản Giáo dục VN', 46000, 21, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (40009, N'Sách giáo khoa', N'Toán 4', N'Nhà xuất bản Giáo dục VN', 44000, 3, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (40010, N'Sách giáo khoa', N'Toán 5', N'Nhà xuất bản Giáo dục VN', 22000, 5, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (40011, N'Sách giáo khoa', N'Vật lý 8', N'Nhà xuất bản Giáo dục VN', 16000, 12, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (40012, N'Sách giáo khoa', N'Vật lý 9', N'Nhà xuất bản Giáo dục VN', 52000, 8, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (40013, N'Sách giáo khoa', N'Hoá học 8', N'Nhà xuất bản Giáo dục VN', 27000, 7, 3, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (40014, N'Sách giáo khoa', N'Hoá học 9', N'Nhà xuất bản Giáo dục VN', 15000, 7, 2, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (40015, N'Sách giáo khoa', N'Lịch sử', N'Nhà xuất bản Giáo dục VN', 18000, 7, 2, N'Cũ')

INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (50001 , N'Sách chính trị', N'Chính trị Luận', N'Aristotle', 35000, 7, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (50002, N'Sách chính trị', N'Khế ước xã hội', N'Rousseau', 56000, 15, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (50003, N'Sách chính trị', N'Cộng Hoà', N'Plato', 30000, 10, 3, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (50004, N'Sách chính trị', N'Thế giới cho đến ngày hôm qua', N'Jared Diamond', 26000, 12, 2, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (50005, N'Sách chính trị', N'Sụp đổ', N'Jared Diamond', 31000, 20, 2, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (50006, N'Sách chính trị', N'Phải trái đúng sai', N'Michael SANDEL', 56000, 17, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (50007, N'Sách chính trị', N'33 Chiến lược của chiến tranh', N'Robert Greene', 56000, 9, 3, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (50008, N'Sách chính trị', N'Xứ đàng trong', N'Li Tana', 46000, 21, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (50009, N'Sách chính trị', N'Đối thoại với thượng đế', N'Neale Donald Walsch', 44000, 3, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (50010, N'Sách chính trị', N'Tiền không mua được gì', N'Michael SANDEL', 32000, 5, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (50011, N'Sách chính trị', N'Gã khổng lồ mất ngủ', NULL, 56000, 12, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (50012, N'Sách chính trị', N'Hồi kí chính trị', N'Tun Dr Mahathir Mohamad', 52000, 8, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (50013, N'Sách chính trị', N'Tiểu Sử David Ben – Gurion', N'Michael Zar-Bohar', 47000, 7, 3, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (50014, N'Sách chính trị', N'Tuyên ngôn của Đảng Cộng sản', N'C.Mác và ĂngGhen', 150000, 7, 2, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (50015, N'Sách chính trị', N'Bàn về tự do', N'John Stuart Mill', 80000, 7, 2, N'Cũ')

INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (60001, N'Khoa học viễn tưởng', N'Xứ cát', N'Frank Herbert', 35000, 7, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (60002, N'Khoa học viễn tưởng', N'Trạm tín hiệu số 23', N'Hugh Howey', 56000, 15, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (60003, N'Khoa học viễn tưởng', N'Animorphs', N'K.A. Applegate', 30000, 10, 3, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (60004, N'Khoa học viễn tưởng', N'Tam thể', N'Lưu Từ Hân', 26000, 12, 2, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (60005, N'Khoa học viễn tưởng', N'Neuromancer', N'William Gibson', 31000, 20, 2, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (60006, N'Khoa học viễn tưởng', N'The Forever War', N'Joe Haldeman', 56000, 17, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (60007, N'Khoa học viễn tưởng', N'Thế giới mới tươi đẹp', N'Aldous Huxley', 56000, 9, 3, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (60008, N'Khoa học viễn tưởng', N'Cỗ máy thời gian', N'H.G. Wells​', 46000, 21, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (60009, N'Khoa học viễn tưởng', N'Người về từ Sao Hoả', N'Andy Weir​', 44000, 3, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (60010, N'Khoa học viễn tưởng', N'451 F', N'Ray Bradbury', 32000, 5, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (60011, N'Khoa học viễn tưởng', N'Hai vạn dặm dười đáy biển', N'Jules Verme', 56000, 12, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (60012, N'Khoa học viễn tưởng', N'Tiên phong lên mặt trăng', N'H.G. Wells', 52000, 8, 1, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (60013, N'Khoa học viễn tưởng', N'Cuộc đấu của Ender', N'Orson Scott', 47000, 7, 3, N'Cũ')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (60014, N'Khoa học viễn tưởng', N'All Around the Moon', N'Jules Verme', 150000, 7, 2, N'Mới')
INSERT into Sach(MaSach,TheLoai, TenSach, TacGia, GiaSach, SoLuongTon,DoHot, TinhTrang)
VALUES (60015, N'Khoa học viễn tưởng', N'Bản đồ mây', N'David Mitchell', 80000, 7, 2, N'Cũ')


--PhieuMuon
Insert into PhieuMuon (MaPhieu, MaKH, MaSach, NgayMuon, NgayPhaiTra)
VALUES (1, 1, 20001,'2/21/2018', '2/26/2018')
Insert into PhieuMuon (MaPhieu, MaKH, MaSach, NgayMuon, NgayPhaiTra)
VALUES (1, 1, 30002,'5/12/2018', '5/18/2018')
Insert into PhieuMuon (MaPhieu, MaKH, MaSach, NgayMuon, NgayPhaiTra)
VALUES (2, 3, 20002, '8/16/2018', '8/22/2018')
Insert into PhieuMuon (MaPhieu, MaKH, MaSach, NgayMuon, NgayPhaiTra)
VALUES (3, 2, 50001, '8/16/2018', '8/22/2018')
Insert into PhieuMuon (MaPhieu, MaKH, MaSach, NgayMuon, NgayPhaiTra)
VALUES (4, 5, 40008, '2/21/2018', '2/28/2018')

--PhieuTra
Insert into PhieuTra (MaPhieu, MaKH, MaSach, NgayTra, TongTien)
VALUES (1, 1, 20001, '2/26/2018', NULL)
Insert into PhieuTra (MaPhieu, MaKH, MaSach, NgayTra, TongTien)
VALUES (2, 1, 30002, '5/18/2018', NULL)
Insert into PhieuTra (MaPhieu, MaKH, MaSach, NgayTra, TongTien)
VALUES (3, 3,  20002, '8/22/2018', NULL)
Insert into PhieuTra (MaPhieu, MaKH, MaSach, NgayTra, TongTien)
VALUES (4, 2,  50001, '8/23/2018',NULL)
Insert into PhieuTra (MaPhieu, MaKH, MaSach, NgayTra, TongTien)
VALUES (5, 5,  40008, '2/28/2018',NULL)

 
