﻿namespace Login
{
    partial class FormDangKi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DKName = new System.Windows.Forms.Label();
            this.DKEmail = new System.Windows.Forms.Label();
            this.DKSĐT = new System.Windows.Forms.Label();
            this.DKTaiKhoan = new System.Windows.Forms.Label();
            this.DKMatKhau = new System.Windows.Forms.Label();
            this.DKMatKhauAgain = new System.Windows.Forms.Label();
            this.txtHoTen = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtTaiKhoan = new System.Windows.Forms.TextBox();
            this.txtMatKhau = new System.Windows.Forms.TextBox();
            this.txtXNMatKhau = new System.Windows.Forms.TextBox();
            this.btnDKTaiKhoan = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // DKName
            // 
            this.DKName.AutoSize = true;
            this.DKName.Location = new System.Drawing.Point(29, 29);
            this.DKName.Name = "DKName";
            this.DKName.Size = new System.Drawing.Size(43, 13);
            this.DKName.TabIndex = 0;
            this.DKName.Text = "Họ Tên";
            // 
            // DKEmail
            // 
            this.DKEmail.AutoSize = true;
            this.DKEmail.Location = new System.Drawing.Point(29, 74);
            this.DKEmail.Name = "DKEmail";
            this.DKEmail.Size = new System.Drawing.Size(32, 13);
            this.DKEmail.TabIndex = 1;
            this.DKEmail.Text = "Email";
            // 
            // DKSĐT
            // 
            this.DKSĐT.AutoSize = true;
            this.DKSĐT.Location = new System.Drawing.Point(29, 123);
            this.DKSĐT.Name = "DKSĐT";
            this.DKSĐT.Size = new System.Drawing.Size(70, 13);
            this.DKSĐT.TabIndex = 2;
            this.DKSĐT.Text = "Số điện thoại";
            // 
            // DKTaiKhoan
            // 
            this.DKTaiKhoan.AutoSize = true;
            this.DKTaiKhoan.Location = new System.Drawing.Point(29, 176);
            this.DKTaiKhoan.Name = "DKTaiKhoan";
            this.DKTaiKhoan.Size = new System.Drawing.Size(56, 13);
            this.DKTaiKhoan.TabIndex = 3;
            this.DKTaiKhoan.Text = "Tài Khoản";
            // 
            // DKMatKhau
            // 
            this.DKMatKhau.AutoSize = true;
            this.DKMatKhau.Location = new System.Drawing.Point(29, 224);
            this.DKMatKhau.Name = "DKMatKhau";
            this.DKMatKhau.Size = new System.Drawing.Size(52, 13);
            this.DKMatKhau.TabIndex = 4;
            this.DKMatKhau.Text = "Mật khẩu";
            // 
            // DKMatKhauAgain
            // 
            this.DKMatKhauAgain.AutoSize = true;
            this.DKMatKhauAgain.Location = new System.Drawing.Point(29, 274);
            this.DKMatKhauAgain.Name = "DKMatKhauAgain";
            this.DKMatKhauAgain.Size = new System.Drawing.Size(93, 13);
            this.DKMatKhauAgain.TabIndex = 5;
            this.DKMatKhauAgain.Text = "Nhập lại mật khẩu";
            // 
            // txtHoTen
            // 
            this.txtHoTen.Location = new System.Drawing.Point(147, 26);
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Size = new System.Drawing.Size(183, 20);
            this.txtHoTen.TabIndex = 6;
            this.txtHoTen.TextChanged += new System.EventHandler(this.txtHoTen_TextChanged);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(147, 71);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(183, 20);
            this.txtEmail.TabIndex = 7;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(147, 116);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(183, 20);
            this.txtPhone.TabIndex = 8;
            // 
            // txtTaiKhoan
            // 
            this.txtTaiKhoan.Location = new System.Drawing.Point(147, 169);
            this.txtTaiKhoan.Name = "txtTaiKhoan";
            this.txtTaiKhoan.Size = new System.Drawing.Size(183, 20);
            this.txtTaiKhoan.TabIndex = 9;
            // 
            // txtMatKhau
            // 
            this.txtMatKhau.Location = new System.Drawing.Point(147, 217);
            this.txtMatKhau.Name = "txtMatKhau";
            this.txtMatKhau.Size = new System.Drawing.Size(183, 20);
            this.txtMatKhau.TabIndex = 10;
            // 
            // txtXNMatKhau
            // 
            this.txtXNMatKhau.Location = new System.Drawing.Point(147, 267);
            this.txtXNMatKhau.Name = "txtXNMatKhau";
            this.txtXNMatKhau.Size = new System.Drawing.Size(183, 20);
            this.txtXNMatKhau.TabIndex = 11;
            // 
            // btnDKTaiKhoan
            // 
            this.btnDKTaiKhoan.Location = new System.Drawing.Point(265, 341);
            this.btnDKTaiKhoan.Name = "btnDKTaiKhoan";
            this.btnDKTaiKhoan.Size = new System.Drawing.Size(89, 42);
            this.btnDKTaiKhoan.TabIndex = 12;
            this.btnDKTaiKhoan.Text = "Đăng kí";
            this.btnDKTaiKhoan.UseVisualStyleBackColor = true;
            this.btnDKTaiKhoan.Click += new System.EventHandler(this.btnDKTaiKhoan_Click);
            // 
            // FormDangKi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 415);
            this.Controls.Add(this.btnDKTaiKhoan);
            this.Controls.Add(this.txtXNMatKhau);
            this.Controls.Add(this.txtMatKhau);
            this.Controls.Add(this.txtTaiKhoan);
            this.Controls.Add(this.txtPhone);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtHoTen);
            this.Controls.Add(this.DKMatKhauAgain);
            this.Controls.Add(this.DKMatKhau);
            this.Controls.Add(this.DKTaiKhoan);
            this.Controls.Add(this.DKSĐT);
            this.Controls.Add(this.DKEmail);
            this.Controls.Add(this.DKName);
            this.Name = "FormDangKi";
            this.Text = "FormDangKi";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label DKName;
        private System.Windows.Forms.Label DKEmail;
        private System.Windows.Forms.Label DKSĐT;
        private System.Windows.Forms.Label DKTaiKhoan;
        private System.Windows.Forms.Label DKMatKhau;
        private System.Windows.Forms.Label DKMatKhauAgain;
        private System.Windows.Forms.TextBox txtHoTen;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtTaiKhoan;
        private System.Windows.Forms.TextBox txtMatKhau;
        private System.Windows.Forms.TextBox txtXNMatKhau;
        private System.Windows.Forms.Button btnDKTaiKhoan;
    }
}