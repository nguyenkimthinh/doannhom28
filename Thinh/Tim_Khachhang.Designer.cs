﻿namespace KhachHang_Thinh
{
    partial class KhachHang_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MAKH_textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.KHACHHANG_dataGridView = new System.Windows.Forms.DataGridView();
            this.MAKH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HOTEN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NGAYSINH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GIOITINH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DIACHI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TENTK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Find_button = new System.Windows.Forms.Button();
            this.DIACHI_textBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.HOTEN_textBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.KHACHHANG_dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // MAKH_textBox
            // 
            this.MAKH_textBox.Location = new System.Drawing.Point(126, 25);
            this.MAKH_textBox.Name = "MAKH_textBox";
            this.MAKH_textBox.Size = new System.Drawing.Size(280, 26);
            this.MAKH_textBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Mã Khách Hàng";
            // 
            // KHACHHANG_dataGridView
            // 
            this.KHACHHANG_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.KHACHHANG_dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MAKH,
            this.HOTEN,
            this.NGAYSINH,
            this.GIOITINH,
            this.DIACHI,
            this.TENTK});
            this.KHACHHANG_dataGridView.Location = new System.Drawing.Point(17, 156);
            this.KHACHHANG_dataGridView.Name = "KHACHHANG_dataGridView";
            this.KHACHHANG_dataGridView.RowTemplate.Height = 28;
            this.KHACHHANG_dataGridView.Size = new System.Drawing.Size(771, 286);
            this.KHACHHANG_dataGridView.TabIndex = 6;
            // 
            // MAKH
            // 
            this.MAKH.DataPropertyName = "MAKH";
            this.MAKH.HeaderText = "Mã KH";
            this.MAKH.Name = "MAKH";
            // 
            // HOTEN
            // 
            this.HOTEN.DataPropertyName = "HOTEN";
            this.HOTEN.HeaderText = "Họ và Tên";
            this.HOTEN.Name = "HOTEN";
            this.HOTEN.Width = 228;
            // 
            // NGAYSINH
            // 
            this.NGAYSINH.DataPropertyName = "NGAYSINH";
            this.NGAYSINH.HeaderText = "Ngày Sinh";
            this.NGAYSINH.Name = "NGAYSINH";
            // 
            // GIOITINH
            // 
            this.GIOITINH.DataPropertyName = "GIOITINH";
            this.GIOITINH.HeaderText = "Nam";
            this.GIOITINH.Name = "GIOITINH";
            this.GIOITINH.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.GIOITINH.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // DIACHI
            // 
            this.DIACHI.DataPropertyName = "DIACHI";
            this.DIACHI.HeaderText = "Địa Chỉ";
            this.DIACHI.Name = "DIACHI";
            // 
            // TENTK
            // 
            this.TENTK.DataPropertyName = "TENTK";
            this.TENTK.HeaderText = "Tài Khoản";
            this.TENTK.Name = "TENTK";
            // 
            // Find_button
            // 
            this.Find_button.Location = new System.Drawing.Point(498, 53);
            this.Find_button.Name = "Find_button";
            this.Find_button.Size = new System.Drawing.Size(159, 54);
            this.Find_button.TabIndex = 7;
            this.Find_button.Text = "Tìm kiếm";
            this.Find_button.UseVisualStyleBackColor = true;
            this.Find_button.Click += new System.EventHandler(this.Find_button_Click);
            // 
            // DIACHI_textBox
            // 
            this.DIACHI_textBox.Location = new System.Drawing.Point(126, 110);
            this.DIACHI_textBox.Name = "DIACHI_textBox";
            this.DIACHI_textBox.Size = new System.Drawing.Size(280, 26);
            this.DIACHI_textBox.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Địa Chỉ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Họ và Tên";
            // 
            // HOTEN_textBox
            // 
            this.HOTEN_textBox.Location = new System.Drawing.Point(126, 67);
            this.HOTEN_textBox.Name = "HOTEN_textBox";
            this.HOTEN_textBox.Size = new System.Drawing.Size(280, 26);
            this.HOTEN_textBox.TabIndex = 1;
            // 
            // KhachHang_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Find_button);
            this.Controls.Add(this.KHACHHANG_dataGridView);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DIACHI_textBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.HOTEN_textBox);
            this.Controls.Add(this.MAKH_textBox);
            this.Name = "KhachHang_Form";
            this.Text = "Khách Hàng";
            this.Load += new System.EventHandler(this.KhachHang_Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.KHACHHANG_dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox MAKH_textBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView KHACHHANG_dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn MAKH;
        private System.Windows.Forms.DataGridViewTextBoxColumn HOTEN;
        private System.Windows.Forms.DataGridViewTextBoxColumn NGAYSINH;
        private System.Windows.Forms.DataGridViewTextBoxColumn GIOITINH;
        private System.Windows.Forms.DataGridViewTextBoxColumn DIACHI;
        private System.Windows.Forms.DataGridViewTextBoxColumn TENTK;
        private System.Windows.Forms.Button Find_button;
        private System.Windows.Forms.TextBox DIACHI_textBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox HOTEN_textBox;
    }
}

