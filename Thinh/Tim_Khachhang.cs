﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;

namespace KhachHang_Thinh
{
    public partial class KhachHang_Form : Form
    {
        public KhachHang_Form()
        {
            InitializeComponent();
        }

        SqlConnection connect = new SqlConnection(@"Data Source=THINH-LAPTOP;Initial Catalog=LIBRARY_MANANGER;Integrated Security=True");

        void runSqlCommand(string sql)
        {
            connect.Open(); //mở kết nối
            SqlCommand cmd = new SqlCommand(sql, connect); //thực hiện câu lệnh
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd); //biến lưu dữ liệu lấy đc
            DataTable dt = new DataTable(); //biến table
            da.Fill(dt);
            KHACHHANG_dataGridView.DataSource = dt;
            connect.Close();
        }
           

        private void KhachHang_Form_Load(object sender, EventArgs e)
        {
            string sql = "select * from KHACHHANG"; //câu lệnh sql
            runSqlCommand(sql);
        }

        private void Find_button_Click(object sender, EventArgs e)
        {
            string input = MAKH_textBox.Text;

            string sql = "select * from KHACHHANG where ";  //câu lệnh sql

            if (input.Length > 0) sql += "MAKH ='" + input + "'";
            else
            {
                input = HOTEN_textBox.Text;
                if (input.Length > 0) sql += "HOTEN like N'%" + input + "%'";
                else
                {
                    input = DIACHI_textBox.Text;
                    if (input.Length > 0) sql += "DIACHI like N'%" + input + "%'";
                }
            }

            //xử lý 1 lệnh và lấy dữ liệu trả về
            runSqlCommand(sql);
        }
    }
}
