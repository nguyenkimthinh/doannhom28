﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLy_ThuVien.BUS;

namespace QuanLy_ThuVien
{
    public partial class Find : Form
    {
        Sach_BUS sachBUS = new Sach_BUS();

        public Find()
        {
            InitializeComponent();
        }

        private void findClick(object sender, MouseEventArgs e)
        {
           if(FindBox.Text=="")
            {
                DataGrid.DataSource = sachBUS.GetList();
            }
            else
            {
                if(choiceBox.Text == "Mã Sách")
                {
                    DataGrid.DataSource = sachBUS.TimKiem(FindBox.Text, choiceBox.Text);
                }
                if (choiceBox.Text == "Tên Sách")
                {
                    DataGrid.DataSource = sachBUS.TimKiem(FindBox.Text, choiceBox.Text);
                }
                if (choiceBox.Text == "Tác Giả")
                {
                    DataGrid.DataSource = sachBUS.TimKiem(FindBox.Text, choiceBox.Text);
                }
                if (choiceBox.Text == "Thể Loại")
                {
                    DataGrid.DataSource = sachBUS.TimKiem(FindBox.Text, choiceBox.Text);
                }
                if (choiceBox.Text == "Nhà Xuất Bản")
                {
                    DataGrid.DataSource = sachBUS.TimKiem(FindBox.Text, choiceBox.Text);
                }
                if (choiceBox.Text == "Giá Sách")
                {
                    DataGrid.DataSource = sachBUS.TimKiem(FindBox.Text, choiceBox.Text);
                }
                if (choiceBox.Text == "Tình Trạng")
                {
                    DataGrid.DataSource = sachBUS.TimKiem(FindBox.Text, choiceBox.Text);
                }
            }
            resetData();
        }

        private void sACHBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.sACHBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.qLThuVienDataSet);

        }

        private void sACHBindingNavigatorSaveItem_Click_1(object sender, EventArgs e)
        {
            this.Validate();
            this.sACHBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.qLThuVienDataSet);

        }

        private void resetData()
        {
            DataGrid.Rows.Clear();
            DataGrid.Refresh();
            DataGrid.DataSource = sachBUS.GetList();
        }

        private void Find_Load(object sender, EventArgs e)
        { 
            DataGrid.DataSource = sachBUS.GetList();
        }

        //xóa các sách tìm được
        private void DeleteClick(object sender, MouseEventArgs e)
        {
            string viewBookID;
            for (int col = 0; col < DataGrid.Rows[0].Cells.Count; col++)
            {
                viewBookID = DataGrid.Rows[0].Cells[col].Value.ToString();
                sachBUS.Xoa(viewBookID);
            }
            resetData();
        }
    }
}
