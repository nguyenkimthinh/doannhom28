﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLy_ThuVien.BUS;
using QuanLy_ThuVien.DTO;

namespace QuanLy_ThuVien
{
    public partial class Update : Form
    {
        public Update()
        {
            InitializeComponent();
        }

        private void sACHBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.sACHBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.qLThuVienDataSet);

        }

        private void Update_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qLThuVienDataSet.SACH' table. You can move, or remove it, as needed.
            this.sACHTableAdapter.Fill(this.qLThuVienDataSet.SACH);

        }
        Sach_BUS sachBus = new Sach_BUS();

        private void AddBttClick(object sender, EventArgs e)
        {
            Sach newBook = new Sach();
            Random randNum = new Random();
            newBook.MaSach = randNum.Next(0, 1000).ToString();
            newBook.GiaSach = int.Parse(giaSachText.Text);
            newBook.NhaXuatBan = nhaXuatBanText.Text;
            newBook.SoLuong = int.Parse(soLuongText.Text);
            newBook.TacGia = tacGiaText.Text;
            newBook.TenSach = tenSachText.Text;
            newBook.TheLoai = theLoaiText.Text;
            newBook.TinhTrang = tinhTrangText.Text;
            sachBus.Them(newBook);
        }
    }
}
