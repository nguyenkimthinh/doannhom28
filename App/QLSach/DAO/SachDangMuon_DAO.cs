﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace QuanLy_ThuVien.DAO
{
    class SachDangMuon_DAO : DataProvider
    {
        public DataTable loadSach()
        {
            string sqlString = @"select * from PHIEUMUON,SACH where PHIEUMUON.MaSach = SACH.MaSach";
            return GetData(sqlString);
        }
    }
}
