﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLy_ThuVien.DAO;
using System.Data;

namespace QuanLy_ThuVien.BUS
{
    class SachDangMuon_BUS
    {
        SachDangMuon_DAO sachdangmuonDao = new SachDangMuon_DAO();
        public DataTable GetList()
        {
            return sachdangmuonDao.loadSach();
        }
    }
}
