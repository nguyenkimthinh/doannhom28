﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLy_ThuVien.DAO;
using System.Data;

namespace QuanLy_ThuVien.BUS
{
    class SachKhongSuDung_BUS
    {
        SachKhongSuDung_DAO sachkhongsudungDao = new SachKhongSuDung_DAO();
        public DataTable GetList()
        {
            return sachkhongsudungDao.loadSach();
        }
    }
}
