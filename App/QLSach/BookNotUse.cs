﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLy_ThuVien.BUS;

namespace QuanLy_ThuVien
{
    public partial class BookNotUse : Form
    {
        SachKhongSuDung_BUS bookNotUse = new SachKhongSuDung_BUS();
        public BookNotUse()
        {
            InitializeComponent();
        }

        private void sACHBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.sACHBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.qLThuVienDataSet);

        }

        private void BookNotUse_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qLThuVienDataSet.SACH' table. You can move, or remove it, as needed.
            this.sACHTableAdapter.Fill(this.qLThuVienDataSet.SACH);
            DataGridView.DataSource = bookNotUse.GetList();
        }
    }
}
