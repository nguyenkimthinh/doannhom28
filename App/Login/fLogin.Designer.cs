﻿namespace Login
{
    partial class QuanLyThuVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labUserName = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.labPassword = new System.Windows.Forms.Label();
            this.checkHienThiPass = new System.Windows.Forms.CheckBox();
            this.Login = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.labInCorrect = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.PicPassword = new System.Windows.Forms.PictureBox();
            this.PicUserName = new System.Windows.Forms.PictureBox();
            this.txtDangKi = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicUserName)).BeginInit();
            this.SuspendLayout();
            // 
            // labUserName
            // 
            this.labUserName.AutoSize = true;
            this.labUserName.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.labUserName.Location = new System.Drawing.Point(151, 21);
            this.labUserName.Name = "labUserName";
            this.labUserName.Size = new System.Drawing.Size(96, 22);
            this.labUserName.TabIndex = 0;
            this.labUserName.Text = "UserName";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(238, 56);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(181, 20);
            this.txtUserName.TabIndex = 1;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(238, 126);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(181, 20);
            this.txtPassword.TabIndex = 2;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // labPassword
            // 
            this.labPassword.AutoSize = true;
            this.labPassword.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.labPassword.Location = new System.Drawing.Point(157, 87);
            this.labPassword.Name = "labPassword";
            this.labPassword.Size = new System.Drawing.Size(90, 22);
            this.labPassword.TabIndex = 3;
            this.labPassword.Text = "Password";
            // 
            // checkHienThiPass
            // 
            this.checkHienThiPass.AutoSize = true;
            this.checkHienThiPass.Location = new System.Drawing.Point(208, 166);
            this.checkHienThiPass.Name = "checkHienThiPass";
            this.checkHienThiPass.Size = new System.Drawing.Size(102, 17);
            this.checkHienThiPass.TabIndex = 4;
            this.checkHienThiPass.Text = "Show Password";
            this.checkHienThiPass.UseVisualStyleBackColor = true;
            this.checkHienThiPass.CheckedChanged += new System.EventHandler(this.checkHienThiPass_CheckedChanged);
            // 
            // Login
            // 
            this.Login.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Login.ForeColor = System.Drawing.Color.Black;
            this.Login.Location = new System.Drawing.Point(208, 235);
            this.Login.Name = "Login";
            this.Login.Size = new System.Drawing.Size(75, 33);
            this.Login.TabIndex = 5;
            this.Login.Text = "Login";
            this.Login.UseVisualStyleBackColor = true;
            this.Login.Click += new System.EventHandler(this.Login_Click);
            // 
            // Cancel
            // 
            this.Cancel.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancel.Location = new System.Drawing.Point(344, 235);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 33);
            this.Cancel.TabIndex = 6;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // labInCorrect
            // 
            this.labInCorrect.AutoSize = true;
            this.labInCorrect.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labInCorrect.ForeColor = System.Drawing.Color.Red;
            this.labInCorrect.Location = new System.Drawing.Point(65, 192);
            this.labInCorrect.Name = "labInCorrect";
            this.labInCorrect.Size = new System.Drawing.Size(0, 19);
            this.labInCorrect.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = global::Login.Properties.Resources.images__1_;
            this.pictureBox1.Location = new System.Drawing.Point(12, 21);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(133, 168);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // PicPassword
            // 
            this.PicPassword.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PicPassword.Image = global::Login.Properties.Resources._1657fb4cb4cc4b4;
            this.PicPassword.Location = new System.Drawing.Point(208, 122);
            this.PicPassword.Name = "PicPassword";
            this.PicPassword.Size = new System.Drawing.Size(32, 24);
            this.PicPassword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PicPassword.TabIndex = 9;
            this.PicPassword.TabStop = false;
            // 
            // PicUserName
            // 
            this.PicUserName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PicUserName.Image = global::Login.Properties.Resources.mail;
            this.PicUserName.Location = new System.Drawing.Point(208, 48);
            this.PicUserName.Name = "PicUserName";
            this.PicUserName.Size = new System.Drawing.Size(32, 28);
            this.PicUserName.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PicUserName.TabIndex = 8;
            this.PicUserName.TabStop = false;
            // 
            // txtDangKi
            // 
            this.txtDangKi.AutoSize = true;
            this.txtDangKi.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDangKi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtDangKi.Location = new System.Drawing.Point(24, 249);
            this.txtDangKi.Name = "txtDangKi";
            this.txtDangKi.Size = new System.Drawing.Size(99, 19);
            this.txtDangKi.TabIndex = 11;
            this.txtDangKi.Text = "Đăng kí mới?";
            this.txtDangKi.Click += new System.EventHandler(this.txtDangKi_Click);
            // 
            // QuanLyThuVien
            // 
            this.AcceptButton = this.Login;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 292);
            this.Controls.Add(this.txtDangKi);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.PicPassword);
            this.Controls.Add(this.PicUserName);
            this.Controls.Add(this.labInCorrect);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.Login);
            this.Controls.Add(this.checkHienThiPass);
            this.Controls.Add(this.labPassword);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.labUserName);
            this.Name = "QuanLyThuVien";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng nhập hệ thống";
            this.Load += new System.EventHandler(this.QuanLyThuVien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicUserName)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labUserName;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label labPassword;
        private System.Windows.Forms.CheckBox checkHienThiPass;
        private System.Windows.Forms.Button Login;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Label labInCorrect;
        private System.Windows.Forms.PictureBox PicUserName;
        private System.Windows.Forms.PictureBox PicPassword;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label txtDangKi;
    }
}

