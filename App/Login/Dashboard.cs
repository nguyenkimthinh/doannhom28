﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using fTimSach.DAO;

namespace fTimSach
{
    public partial class Dashboard : Form
    {
        public Dashboard()
        {
            InitializeComponent();
            
        }
        public void funData(string ID)
        {
            txbMaUser.Text = ID;
        }
        // Tìm và mượn_Click
        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            fMuonSach f = new fMuonSach();
            f.funData(txbMaUser.Text.ToString());
            f.ShowDialog();
            this.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            fMuonSach f = new fMuonSach();
            f.funData(txbMaUser.Text.ToString());
            f.ShowDialog();
            this.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            fXemHistory f = new fXemHistory();
            f.funData(txbMaUser.Text.ToString());
            f.ShowDialog();
            this.Show();
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {
            string ID = txbMaUser.Text.ToString();
            string query = "SELECT PhanQuyen FROM TaiKhoan tk WHERE tk.MaKH = " + ID;
            // 
            DataTable dt = DataProvider.Instance.ExecuteQuery(query);
            int pq = (int)dt.Rows[0][0];
            if (pq==0)
            {
                // manager
                tpKH.Enabled = false;
            }
            else
            {
                // kh
                tpQL.Enabled = false;
            }
        }

        private void button_DangXuat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login.FormInfo f = new Login.FormInfo();
            f.funData(txbMaUser.Text.ToString());
            f.ShowDialog();
            this.Show();
        }
    }
}
