﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using fTimSach.DAO;

namespace fTimSach
{
       
    public partial class fMuonSach : Form
    {
        
        public fMuonSach()
        {
            InitializeComponent();
            //txMaKH.Text = "KH0001";
        }
        #region Method
        public void funData(string ID)
        {
            txbKiemTraID.Text = ID;
        }
        void loadTheloai() // Hiển thị các thể loại
        {
            string query = "SELECT DISTINCT TheLoai FROM Sach";

            cbTheloai.DataSource = DataProvider.Instance.ExecuteQuery(query);

            cbTheloai.DisplayMember = "TheLoai";

        } 
        
        void TraCuu(string loaiTk, string text, string theLoai) // Hàm tìm kiếm sách
        {
            string query = "";
            if (loaiTk == "Tên sách")
                loaiTk = "TenSach";
            else if (loaiTk == "Tên tác giả")
                loaiTk = "TacGia";
            else
                loaiTk = "MaSach";
            if (ckbTheloai.Checked)
                query = string.Format("SELECT MaSach, TenSach, TheLoai, TacGia, GiaSach, DoHot, TinhTrang, SoLuongTon FROM SACH WHERE {0} like N'%{1}%'AND TheLoai = N'{2}'", loaiTk, text, theLoai);
            else
                query = string.Format("SELECT MaSach, TenSach, TheLoai, TacGia, GiaSach, DoHot, TinhTrang, SoLuongTon FROM SACH WHERE {0} like N'%{1}%'", loaiTk, text);
    
                dtgvSach.DataSource = DataProvider.Instance.ExecuteQuery(query);

                dtgvSach.Columns[0].HeaderText = "Mã sách";
                dtgvSach.Columns[1].HeaderText = "Tên sách";
                dtgvSach.Columns[2].HeaderText = "Thể loại";
                dtgvSach.Columns[3].HeaderText = "Tác giả";
                dtgvSach.Columns[4].HeaderText = "Giá sách";
                dtgvSach.Columns[5].HeaderText = "Độ hot";
                dtgvSach.Columns[6].HeaderText = "Tình trạng";
                dtgvSach.Columns[7].HeaderText = "Số lượng tồn";
           
            
        }
        void ThongTinSachDangChon() // Hàm hiển thị thông tin sách đang chọn
        {
            
                txbTensach.Text = dtgvSach.SelectedCells[1].Value.ToString();
                txbMasach.Text = dtgvSach.SelectedCells[0].Value.ToString();
                //txbGia.Text = dtgvSach.SelectedCells[4].Value.ToString();
               
            
        } 
      
        void loadThongTinDocGia() // Hàm hiển thị thông tin độc giả
        {

            string MaKH = txMaKH.Text.ToString();
            int i;
           
            if (!Int32.TryParse(MaKH, out i))
            {
                i = -1;
            }
            if (i == -1)
            {
                MessageBox.Show("Mã khách hàng không tồn tại.");
            }
            else
            {
                DataTable data = DataProvider.Instance.ExecuteQuery(string.Format("SELECT Hoten, MaKH FROM TaiKhoan WHERE MaKH = {0}", MaKH));
                if (data.Rows.Count > 0)
                {
                    txbHoten.Text = data.Rows[0][0].ToString();
                    txbMakh.Text = data.Rows[0][1].ToString();
                }
                else
                    MessageBox.Show("Mã khách hàng không tồn tại.");
            }
        } // ok
        void ktDisable() // Hàm kiểm tra listview để disable 2 nút Remove
        {
            if (lvEmpty())
            {
                btnBohet.Enabled = false;
                btnRemove.Enabled = false;
            }
            else
            {
                foreach (ListViewItem item in listSach.Items)
                {
                    if (item.Selected)
                        return;
                }
                btnBohet.Enabled = false;
                btnRemove.Enabled = false;
            }
        }
        bool lvEmpty() // Hàm kiểm tra listview rỗng
        {
            if (listSach.Items.Count == 0)
            {
                return true;
            }
            return false;
        }
        #endregion
        
        #region Events
        private void btnXemlichsumuon_Click(object sender, EventArgs e)
        {
            fXemHistory fxemhistory = new fXemHistory();
            fxemhistory.funData(this.txbKiemTraID.Text.ToString());
            fxemhistory.ShowDialog();
        }
       
        private void btnMuon_Click_1(object sender, EventArgs e)
        {
            if (listSach.Items.Count==0)
            {
                MessageBox.Show("Danh sách mượn trống!", "Thông báo");
                return;
            }
            if (MessageBox.Show("Xác nhận phiếu mượn", "Xác nhận", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
            {
                if ((dtpkNgaytra.Value <= dtpkNgaymuon.Value) )
                {
                    
                    MessageBox.Show("Ngày trả không hợp lệ. Vui lòng chọn lại.", "Thông báo");
                    return;
                }
                TimeSpan time = dtpkNgaytra.Value - dtpkNgaymuon.Value;
                int CountDay = time.Days;
                if (CountDay > 90)
                {
                    MessageBox.Show("Không được phép mượn quá 90 ngày", "Thông báo");
                    return;
                }
                DataTable dt = DataProvider.Instance.ExecuteQuery("SELECT MAX(MaPhieu) FROM PhieuMuon"); // result int
                string maPhieu = dt.Rows[0][0].ToString();
                if (maPhieu != "")
                {
          
                    int mptemp = Int32.Parse(maPhieu) + 1;
                    maPhieu = mptemp.ToString();
                }
                else
                    maPhieu = "0";
                
                string maDocGia = txbMakh.Text;
                
                string ngayMuon = dtpkNgaymuon.Value.Year.ToString() + "/" + dtpkNgaymuon.Value.Month.ToString() + "/" + dtpkNgaymuon.Value.Day.ToString();
                string ngayTra = dtpkNgaytra.Value.Year.ToString() + "/" + dtpkNgaytra.Value.Month.ToString() + "/" + dtpkNgaytra.Value.Day.ToString();
                foreach (ListViewItem itemrow in listSach.Items)
                {
                   string maSach = itemrow.SubItems[1].Text;
                    //-- insert Phieu muon
                   int kt = DataProvider.Instance.ExecuteNonQuery(string.Format("INSERT INTO PHIEUMUON VALUES ({0},{1},{2},'{3}','{4}')", maPhieu, maDocGia, maSach, ngayMuon, ngayTra ));
                    //-- update so luong sach
                   kt = DataProvider.Instance.ExecuteNonQuery(string.Format("UPDATE SACH SET SoLuongTon = SoLuongTon - 1 WHERE MaSach = {0}", maSach));
                }
                MessageBox.Show(string.Format("Mã Phiếu mượn là {0}.", maPhieu), "Thành công");
              
                listSach.Items.Clear();
         
            }
        }
        private void fTimSach_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn thoát chương trình?", "Thông báo", MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK)
                e.Cancel = true;
        }
        private void btnTim_Click(object sender, EventArgs e)
        {
            if (txbInfo.Text != "")
            {
                TraCuu(cbChedotim.Text, txbInfo.Text, cbTheloai.Text);
                if (dtgvSach.Rows.Count == 0)
                {
                    txbMasach.Text = "";
                    txbTensach.Text = "";
                }
            }
            else
            {
                if (ckbTheloai.Checked)
                {
                    TraCuu(cbChedotim.Text, txbInfo.Text, cbTheloai.Text);
                    if (dtgvSach.Rows.Count == 0)
                    {
                        txbMasach.Text = "";
                        txbTensach.Text = "";
                    }
                }
                else
                MessageBox.Show("Chưa nhập thông tin sách", "Thông báo");
            }
                
        }
       
        private void button1_Click(object sender, EventArgs e)
        {
            if (txMaKH.Text=="")
            {
                MessageBox.Show("Bạn chưa nhập mã Khách hàng","Thông báo");
            }
            else
            loadThongTinDocGia();
            
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txbMakh.Text == "")
            {
                MessageBox.Show("Vui lòng nhập mã Khách hàng để thêm sách", "Thông báo");
                return;
            }
            if (txbMasach.Text == "")
            {
                MessageBox.Show("Vui lòng chọn sách ", "Thông báo");
                return;
            }
            int slTon = Int32.Parse(dtgvSach.SelectedCells[5].Value.ToString());

            if (slTon == 0)
            {
                MessageBox.Show("Sách đã hết. Không thể mượn", "Thông báo");
                return;
            }
            string madg = txbMakh.Text;
            string masach = txbMasach.Text.ToString();
            //string giasach = txbGia.Text.ToString();
            ListViewItem sach = new ListViewItem(txbTensach.Text.ToString());
            bool trung = false;
            foreach (ListViewItem item in listSach.Items)
            {
                if (item.Text == sach.Text)
                    trung = true;
            }
            if (trung)
            {
                MessageBox.Show("Sách đã có trong phiếu mượn. Không thể thêm", "Thông báo");
            }
            else
            {
                sach.SubItems.Add(masach);
                listSach.Items.Add(sach);
                
            }
        }
        private void fTimSach_Load(object sender, EventArgs e)
        {
            cbChedotim.SelectedIndex = 0;
            
            ktDisable();
            // set giá trị default datetime
            dtpkNgaymuon.Value = DateTime.Now;
            dtpkNgaytra.Value = DateTime.Now;
            //----
            loadTheloai(); // hiển thị các thể loại
            // Kiểm tra ID là Quản lí hay Khách hàng
            //txbKiemTraID.Text = "2";
            string query = "SELECT PhanQuyen FROM TaiKhoan tk WHERE tk.MaKH = " + txbKiemTraID.Text.ToString();
            DataTable dt = DataProvider.Instance.ExecuteQuery(query);
            if (dt.Rows[0][0].ToString()=="0")
            {
                // Quan li
                //MessageBox.Show("Bạn là quản lí");
                pnQuanLi.Visible = true;
            }
            else
            {
                // khach hang
                // MessageBox.Show("Bạn là khách hàng");
                pnQuanLi.Visible = false;
                string maKH = txbKiemTraID.Text.ToString();
                DataTable data = DataProvider.Instance.ExecuteQuery(string.Format("SELECT Hoten, MaKH FROM TaiKhoan WHERE MaKH = {0}", maKH));
                if (data.Rows.Count > 0)
                {
                    txbHoten.Text = data.Rows[0][0].ToString();
                    txbMakh.Text = data.Rows[0][1].ToString();
                }
            }
        }
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (!lvEmpty())
            {
                listSach.SelectedItems[0].Remove();
            }
            else
                MessageBox.Show("Danh sách mượn rỗng", "Thông báo");
            ktDisable();
        }
        private void btnBohet_Click(object sender, EventArgs e)
        {
            if (!lvEmpty())
            {
                listSach.Items.Clear();
                ktDisable();
            }
            else
                MessageBox.Show("Danh sách mượn rỗng", "Thông báo");
        }
        private void listSach_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            btnBohet.Enabled = true;
            btnRemove.Enabled = true;
        }
        #endregion

        private void dtgvSach_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
            ThongTinSachDangChon();
        }

        private void listSach_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
