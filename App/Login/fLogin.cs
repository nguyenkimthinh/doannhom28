﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using fTimSach;

namespace Login
{
    public partial class QuanLyThuVien : Form
    {
        Dashboard dash;
        FormDangKi DK;
        string Connect = @"Data Source=THINH-LAPTOP;Initial Catalog=QLThuVien;Integrated Security=True";
        SqlConnection Conn;
        SqlCommand Comm;
        public QuanLyThuVien()
        {
            InitializeComponent();
        }
        private void QuanLyThuVien_Load(object sender, EventArgs e)
        {

        }

        private void Login_Click(object sender, EventArgs e)
        {
            try
            {
                Conn = new SqlConnection(Connect);
                Conn.Open();
                string Sql = "SELECT COUNT(*) FROM [QLThuVien].[dbo].[TaiKhoan] WHERE UserName=@TaiKhoan AND MyPassword=@MatKhau";
                Comm = new SqlCommand(Sql, Conn);
                Comm.Parameters.Add(new SqlParameter("@TaiKhoan", txtUserName.Text));
                Comm.Parameters.Add(new SqlParameter("@MatKhau", txtPassword.Text));
                int x = (int)Comm.ExecuteScalar();
                // Lấy ID 
                
                if (x == 1)
                {
                    //Đăng nhập thành công.
                    //MessageBox.Show("Success!");
                    Sql = string.Format("SELECT MaKH FROM TaiKhoan tk WHERE tk.UserName = '{0}'", txtUserName.Text.ToString());
                    Comm = new SqlCommand(Sql, Conn);
                    int dt = (int)Comm.ExecuteScalar();
                    this.Hide();
                    txtPassword.Text = "";
                    dash = new Dashboard();
                    dash.funData(dt.ToString());
                    dash.ShowDialog();
                    this.Show();
                }
                else
                {
                    //Đăng nhập thất bai.
                    //MessageBox.Show("Fail!!!");
                    labInCorrect.Text = "Invalid Username or Password. Please enter again!";
                    txtPassword.Text = "";
                    txtPassword.Text = "";
                    txtUserName.Focus();
                }
                Conn.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    
        private void checkHienThiPass_CheckedChanged(object sender, EventArgs e)
        {
            if(checkHienThiPass.Checked)
            {
                txtPassword.UseSystemPasswordChar = false;
            }
            else
            {
                txtPassword.UseSystemPasswordChar = true;
            }
        }

 //Cancel.
        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

 //Đăng kí tài khoản mới.
        private void txtDangKi_Click(object sender, EventArgs e)
        {
            this.Hide();
            DK = new FormDangKi();
            DK.ShowDialog();
            this.Show();

        }
    }
}
