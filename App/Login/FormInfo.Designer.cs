﻿namespace Login
{
    partial class FormInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txbMaUser = new System.Windows.Forms.TextBox();
            this.Sex_TextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Name_TextBox = new System.Windows.Forms.TextBox();
            this.Email_TextBox = new System.Windows.Forms.TextBox();
            this.Num_TextBox = new System.Windows.Forms.TextBox();
            this.Type_TextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(96, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 22);
            this.label1.TabIndex = 10;
            this.label1.Text = "Mã";
            // 
            // txbMaUser
            // 
            this.txbMaUser.Location = new System.Drawing.Point(140, 14);
            this.txbMaUser.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txbMaUser.Name = "txbMaUser";
            this.txbMaUser.ReadOnly = true;
            this.txbMaUser.Size = new System.Drawing.Size(292, 26);
            this.txbMaUser.TabIndex = 9;
            // 
            // Sex_TextBox
            // 
            this.Sex_TextBox.Location = new System.Drawing.Point(140, 86);
            this.Sex_TextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Sex_TextBox.Name = "Sex_TextBox";
            this.Sex_TextBox.ReadOnly = true;
            this.Sex_TextBox.Size = new System.Drawing.Size(292, 26);
            this.Sex_TextBox.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(56, 51);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 22);
            this.label2.TabIndex = 12;
            this.label2.Text = "Họ Tên";
            // 
            // Name_TextBox
            // 
            this.Name_TextBox.Location = new System.Drawing.Point(140, 50);
            this.Name_TextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name_TextBox.Name = "Name_TextBox";
            this.Name_TextBox.ReadOnly = true;
            this.Name_TextBox.Size = new System.Drawing.Size(292, 26);
            this.Name_TextBox.TabIndex = 11;
            // 
            // Email_TextBox
            // 
            this.Email_TextBox.Location = new System.Drawing.Point(140, 122);
            this.Email_TextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Email_TextBox.Name = "Email_TextBox";
            this.Email_TextBox.ReadOnly = true;
            this.Email_TextBox.Size = new System.Drawing.Size(292, 26);
            this.Email_TextBox.TabIndex = 13;
            // 
            // Num_TextBox
            // 
            this.Num_TextBox.Location = new System.Drawing.Point(140, 158);
            this.Num_TextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Num_TextBox.Name = "Num_TextBox";
            this.Num_TextBox.ReadOnly = true;
            this.Num_TextBox.Size = new System.Drawing.Size(292, 26);
            this.Num_TextBox.TabIndex = 14;
            // 
            // Type_TextBox
            // 
            this.Type_TextBox.Location = new System.Drawing.Point(140, 194);
            this.Type_TextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Type_TextBox.Name = "Type_TextBox";
            this.Type_TextBox.ReadOnly = true;
            this.Type_TextBox.Size = new System.Drawing.Size(292, 26);
            this.Type_TextBox.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(47, 86);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 22);
            this.label3.TabIndex = 18;
            this.label3.Text = "Giới tính";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(73, 122);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 22);
            this.label4.TabIndex = 19;
            this.label4.Text = "Email";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(4, 158);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(128, 22);
            this.label5.TabIndex = 20;
            this.label5.Text = "Số điện thoại";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(12, 194);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 22);
            this.label6.TabIndex = 21;
            this.label6.Text = "Phân Quyền";
            // 
            // FormInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 238);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Type_TextBox);
            this.Controls.Add(this.Num_TextBox);
            this.Controls.Add(this.Email_TextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Name_TextBox);
            this.Controls.Add(this.Sex_TextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txbMaUser);
            this.Name = "FormInfo";
            this.Text = "Thông tin tài khoản";
            this.Load += new System.EventHandler(this.FormInfo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txbMaUser;
        private System.Windows.Forms.TextBox Sex_TextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Name_TextBox;
        private System.Windows.Forms.TextBox Email_TextBox;
        private System.Windows.Forms.TextBox Num_TextBox;
        private System.Windows.Forms.TextBox Type_TextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}