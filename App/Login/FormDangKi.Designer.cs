﻿namespace Login
{
    partial class FormDangKi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DKName = new System.Windows.Forms.Label();
            this.DKEmail = new System.Windows.Forms.Label();
            this.DKSĐT = new System.Windows.Forms.Label();
            this.DKTaiKhoan = new System.Windows.Forms.Label();
            this.DKMatKhau = new System.Windows.Forms.Label();
            this.DKMatKhauAgain = new System.Windows.Forms.Label();
            this.txtHoTen = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtTaiKhoan = new System.Windows.Forms.TextBox();
            this.txtMatKhau = new System.Windows.Forms.TextBox();
            this.txtXNMatKhau = new System.Windows.Forms.TextBox();
            this.btnDKTaiKhoan = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtGioiTinh = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // DKName
            // 
            this.DKName.AutoSize = true;
            this.DKName.Location = new System.Drawing.Point(44, 45);
            this.DKName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DKName.Name = "DKName";
            this.DKName.Size = new System.Drawing.Size(61, 20);
            this.DKName.TabIndex = 0;
            this.DKName.Text = "Họ Tên";
            // 
            // DKEmail
            // 
            this.DKEmail.AutoSize = true;
            this.DKEmail.Location = new System.Drawing.Point(44, 168);
            this.DKEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DKEmail.Name = "DKEmail";
            this.DKEmail.Size = new System.Drawing.Size(48, 20);
            this.DKEmail.TabIndex = 1;
            this.DKEmail.Text = "Email";
            // 
            // DKSĐT
            // 
            this.DKSĐT.AutoSize = true;
            this.DKSĐT.Location = new System.Drawing.Point(44, 234);
            this.DKSĐT.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DKSĐT.Name = "DKSĐT";
            this.DKSĐT.Size = new System.Drawing.Size(102, 20);
            this.DKSĐT.TabIndex = 2;
            this.DKSĐT.Text = "Số điện thoại";
            // 
            // DKTaiKhoan
            // 
            this.DKTaiKhoan.AutoSize = true;
            this.DKTaiKhoan.Location = new System.Drawing.Point(44, 305);
            this.DKTaiKhoan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DKTaiKhoan.Name = "DKTaiKhoan";
            this.DKTaiKhoan.Size = new System.Drawing.Size(80, 20);
            this.DKTaiKhoan.TabIndex = 3;
            this.DKTaiKhoan.Text = "Tài Khoản";
            // 
            // DKMatKhau
            // 
            this.DKMatKhau.AutoSize = true;
            this.DKMatKhau.Location = new System.Drawing.Point(44, 368);
            this.DKMatKhau.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DKMatKhau.Name = "DKMatKhau";
            this.DKMatKhau.Size = new System.Drawing.Size(75, 20);
            this.DKMatKhau.TabIndex = 4;
            this.DKMatKhau.Text = "Mật khẩu";
            // 
            // DKMatKhauAgain
            // 
            this.DKMatKhauAgain.AutoSize = true;
            this.DKMatKhauAgain.Location = new System.Drawing.Point(44, 422);
            this.DKMatKhauAgain.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DKMatKhauAgain.Name = "DKMatKhauAgain";
            this.DKMatKhauAgain.Size = new System.Drawing.Size(136, 20);
            this.DKMatKhauAgain.TabIndex = 5;
            this.DKMatKhauAgain.Text = "Nhập lại mật khẩu";
            // 
            // txtHoTen
            // 
            this.txtHoTen.Location = new System.Drawing.Point(220, 40);
            this.txtHoTen.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Size = new System.Drawing.Size(272, 26);
            this.txtHoTen.TabIndex = 6;
            this.txtHoTen.TextChanged += new System.EventHandler(this.txtHoTen_TextChanged);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(220, 168);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(272, 26);
            this.txtEmail.TabIndex = 7;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(220, 229);
            this.txtPhone.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(272, 26);
            this.txtPhone.TabIndex = 8;
            // 
            // txtTaiKhoan
            // 
            this.txtTaiKhoan.Location = new System.Drawing.Point(220, 294);
            this.txtTaiKhoan.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTaiKhoan.Name = "txtTaiKhoan";
            this.txtTaiKhoan.Size = new System.Drawing.Size(272, 26);
            this.txtTaiKhoan.TabIndex = 9;
            // 
            // txtMatKhau
            // 
            this.txtMatKhau.Location = new System.Drawing.Point(220, 357);
            this.txtMatKhau.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtMatKhau.Name = "txtMatKhau";
            this.txtMatKhau.Size = new System.Drawing.Size(272, 26);
            this.txtMatKhau.TabIndex = 10;
            // 
            // txtXNMatKhau
            // 
            this.txtXNMatKhau.Location = new System.Drawing.Point(220, 411);
            this.txtXNMatKhau.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtXNMatKhau.Name = "txtXNMatKhau";
            this.txtXNMatKhau.Size = new System.Drawing.Size(272, 26);
            this.txtXNMatKhau.TabIndex = 11;
            // 
            // btnDKTaiKhoan
            // 
            this.btnDKTaiKhoan.Location = new System.Drawing.Point(398, 525);
            this.btnDKTaiKhoan.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDKTaiKhoan.Name = "btnDKTaiKhoan";
            this.btnDKTaiKhoan.Size = new System.Drawing.Size(134, 65);
            this.btnDKTaiKhoan.TabIndex = 12;
            this.btnDKTaiKhoan.Text = "Đăng kí";
            this.btnDKTaiKhoan.UseVisualStyleBackColor = true;
            this.btnDKTaiKhoan.Click += new System.EventHandler(this.btnDKTaiKhoan_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 105);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 20);
            this.label1.TabIndex = 13;
            this.label1.Text = "Giới tính:";
            // 
            // txtGioiTinh
            // 
            this.txtGioiTinh.Location = new System.Drawing.Point(220, 105);
            this.txtGioiTinh.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtGioiTinh.Name = "txtGioiTinh";
            this.txtGioiTinh.Size = new System.Drawing.Size(272, 26);
            this.txtGioiTinh.TabIndex = 14;
            // 
            // FormDangKi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 638);
            this.Controls.Add(this.txtGioiTinh);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnDKTaiKhoan);
            this.Controls.Add(this.txtXNMatKhau);
            this.Controls.Add(this.txtMatKhau);
            this.Controls.Add(this.txtTaiKhoan);
            this.Controls.Add(this.txtPhone);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtHoTen);
            this.Controls.Add(this.DKMatKhauAgain);
            this.Controls.Add(this.DKMatKhau);
            this.Controls.Add(this.DKTaiKhoan);
            this.Controls.Add(this.DKSĐT);
            this.Controls.Add(this.DKEmail);
            this.Controls.Add(this.DKName);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FormDangKi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormDangKi";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label DKName;
        private System.Windows.Forms.Label DKEmail;
        private System.Windows.Forms.Label DKSĐT;
        private System.Windows.Forms.Label DKTaiKhoan;
        private System.Windows.Forms.Label DKMatKhau;
        private System.Windows.Forms.Label DKMatKhauAgain;
        private System.Windows.Forms.TextBox txtHoTen;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtTaiKhoan;
        private System.Windows.Forms.TextBox txtMatKhau;
        private System.Windows.Forms.TextBox txtXNMatKhau;
        private System.Windows.Forms.Button btnDKTaiKhoan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtGioiTinh;
    }
}