﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using fTimSach.DAO;

namespace Login
{
    public partial class FormInfo : Form
    {
        public FormInfo()
        {
            InitializeComponent();
        }
        
        public void funData(string ID)
        {
            txbMaUser.Text = ID;
        }

        private void FormInfo_Load(object sender, EventArgs e)
        {
            string ID = txbMaUser.Text.ToString();
            string query = "SELECT * FROM TaiKhoan tk WHERE tk.MaKH = " + ID;
            // 
            DataTable dt = DataProvider.Instance.ExecuteQuery(query);
            Name_TextBox.Text = dt.Rows[0][1].ToString();
            Sex_TextBox.Text = dt.Rows[0][2].ToString();
            Email_TextBox.Text = dt.Rows[0][3].ToString();
            Num_TextBox.Text = dt.Rows[0][4].ToString();
            int pq = (int)dt.Rows[0][7];
            if (pq == 0)
            {
                // manager
                Type_TextBox.Text = "Quản lý";
            }
            else
            {
                // kh
                Type_TextBox.Text = "Khách hàng";
            }
        }

        private void Change_Button_Click(object sender, EventArgs e)
        {
            string query = "UPDATE TaiKhoan SET Hoten = '" + Name_TextBox.Text + "', GioiTinh='" + Sex_TextBox.Text + "', Email='" + Email_TextBox.Text
                + "', SDT='"+ Num_TextBox + "' WHERE MaKH = " + txbMaUser.Text;
            int kt = DataProvider.Instance.ExecuteNonQuery(query);
        }
    }
}
